import React from 'react'
import './LikedSong.scss'
import Mountain from '../../public_images/defaultPlaylist.svg';

const LikedSong = ({redirectLikedSong}) => {
    return (
       <div onClick={redirectLikedSong}  id="fast-transition" className="pl-5 col-sm-4 text-center  upwards-content">
            <img src={Mountain} class="rounded custom-height-width-image heart-icon-wrapper" alt="..."/>
            <h4 className="font-family--helvetica">Lagu Favorit</h4>
      </div>
    )
}

export default LikedSong
