import React, {useState, useEffect} from 'react'
import {useLocation, useHistory} from "react-router-dom";
import {createBrowserHistory} from "history";
import Song from '../../Song/Song';
import SongPlayer from '../../AudioPlayer/AudioPlayer';
import Axios from 'axios';
import Toast from 'react-bootstrap/Toast';
import {FaPen, FaSearch} from "react-icons/fa";
import {BASE_URL} from "../../Constant";
import Spinner from 'react-bootstrap/Spinner';

const LikedSongList = ({handleGetCurrSongGlobal, handleGetSongListGlobal, setColorRowIndex, colorRowIndex}) => {
  const location = useLocation();
  const [likedSongs, setLikedSongs] = useState([])
  const [currSong, setCurrSong] = useState({})
  const [currentUser, setCurrentUser] = useState({})
  const [newLikedSongs, setNewLikedSongs] = useState([])
  const [show, setShow] = useState(false)
  const [searchedSong, setSearchedSong] = useState('')

  const {likedMusic, id: userId} = currentUser;
  const locState = location.state
  const history = useHistory();

  const setIndexColorRow = (id) => {
    setColorRowIndex(id);
  }

  useEffect(() => {
    getLoggedInUser()
  }, [])

  const getLoggedInUser = async () => {
    Axios.get(`${BASE_URL}/userProfile/${location.state.userId}`)
      .then(res => {
        setCurrentUser(res.data)
        getLikedSong(res.data.id)
      })
  }

  const handleSetGLobalCurrSong = (songData) => {
    localStorage.removeItem('songList');
    console.log(songData);
    localStorage.setItem('currSong', JSON.stringify(songData))
    if (likedSongs) {
      localStorage.setItem('songList', JSON.stringify(likedSongs))
    }
    handleGetCurrSongGlobal()
    handleGetSongListGlobal()
  }

  const getLikedSong = (userId) => {
    Axios.get(`${BASE_URL}/userProfile/${userId}/likedSongs`)
      .then(res => {
        const data = res.data[0]
        setLikedSongs(data.likedSong)
      })
      .catch(err => {
        console.log(err);
      })
  }

  const goDetail = (song) => {
    history.push(`/songpage/${song._id}`)
  }

  const handleSearchSong = (e) => {
    setSearchedSong(e.target.value)
  }

  return (
    <div style={{width: '100%'}} className='likedSong-page'>
      <div className="row justify-content-between">
        <div className="text-page-user">
        </div>
        <div className="search-song mr-5 mb-3">
          <FaSearch style={{marginRight: '12px'}} className='search-song-likedsong'/>
          <input
            style={{marginLeft: '20px', paddingLeft: '20px'}}
            className="background-transparent border-black text-indent-10px mt-3 search-input"
            placeholder="Cari lagu daerah"
            type="text"
            name="search"
            onChange={handleSearchSong}
          />
        </div>
      </div>
       <div className='likedSong-wrapper break-container'>
        <center><h4 style={{color: 'white', padding: '5px'}}>Lagu Favorit Anda</h4></center>
        <div className="row pt-3">
          <div className="col">
            <h4 style={{color: 'white'}}
                className="custom-font-size-12 likedSong-heading-padding likedSong-heading">Judul Lagu</h4>
          </div>
          <div className="col show">
            <h4 style={{color: 'white'}} className="custom-font-size-12 likedSong-heading">Asal Daerah</h4>
          </div>
          <div className="col new-year show">
            <h4 style={{color: 'white'}}
                className="custom-font-size-12 likedSong-heading likedSong-heading-padding-2">Pencipta</h4>
          </div>

          <div className={'col'}>
          </div>
        </div>
      { likedSongs.length > 0 ?<>
        {
          searchedSong ?
            likedSongs.map((song, i) => {
              console.log(song);
              let title = song.title ? song.title.toLowerCase() : song.songName.toLowerCase()
              if (title.includes(searchedSong.toLowerCase())) {
                return <Song
                  key={song._id}
                  index={i}
                  data={song}
                  goDetail={(song) => goDetail(song)}
                  setCurrSong={setCurrSong}
                  currentUser={currentUser}
                  isLikedPage={true}
                  getLikedSong={getLikedSong}
                  colorRowIndex={colorRowIndex}
                  setColorRowIndex={setColorRowIndex}
                  setShow={setShow}
                  handleSetGLobalCurrSong={handleSetGLobalCurrSong}
                />
              }
            }) :
            likedSongs.map((song, i) => {
              return <Song
                key={song._id}
                index={i}
                data={song}
                goDetail={(song) => goDetail(song)}
                setCurrSong={setCurrSong}
                currentUser={currentUser}
                isLikedPage={true}
                getLikedSong={getLikedSong}
                colorRowIndex={colorRowIndex}
                setColorRowIndex={setColorRowIndex}
                setShow={setShow}
                handleSetGLobalCurrSong={handleSetGLobalCurrSong}
              />
            })
        }
      </> :  <center className='mt-5'> <Spinner variant='light'  animation="border" /> </center>}
      </div>
      <div className="toast-notification position-toast">
        <Toast onClose={() => setShow(false)} show={show} delay={3000} autohide>
          <Toast.Body>Anda berhasil unliked lagu!</Toast.Body>
        </Toast>
      </div>
    </div>
  )
}

export default LikedSongList
