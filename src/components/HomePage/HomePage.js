import React, { useEffect, useState } from 'react';
import './Homepage.scss';
import { getCredential, removeCredential } from '../Utils';
import Axios from 'axios';
import SearchSong from '../SearchSong/SearchSong';
import ImageMapper from 'react-image-mapper';
import PopularSongList from '../PopularSongList/PopularSongList';
import NewUploadedCoverSongList from '../NewUploadedCoverSongList/NewUploadedCoverSongList';
import NewSongList from '../NewSongList/NewSongList';
import OriginSongPlaylist from '../OriginSongPlaylist/OriginSongPlaylist';
import { BASE_URL } from '../Constant'
import Spinner from 'react-bootstrap/Spinner';
import Toast from 'react-bootstrap/Toast';

const HomePage = (props) => {
	const [currentUser, setCurrentUser] = useState({});
	const [hoveredArea, setHoveredArea] = useState(null);
	const [songOrigin, setSongOrigin] = useState([]);
	const [popularSongs, setPopularSongs] = useState([]);
	const [isReportShow, setIsReportShow] = useState(false)
	const [newUploadedCoverSongs, setNewUploadedCoverSongs] = useState([])
	const [newSongs, setNewSongs] = useState([])
	const [originProps, setOriginProps] = useState('')

	const [songList, setSongList] = useState([]);
	const [currSong, setCurrSong] = useState({});
	const [originInfo, setOriginInfo] = useState('')

	useEffect(() => {
		getLoggedInUser();
		getPopularSong()
		getNewUploadedCoverSong()
		getNewSongs()
		props.setCurrUrl('home')
	}, []);
	const getLoggedInUser = () => {
		let user = JSON.parse(getCredential());
		if (user) {
			Axios.get(`${BASE_URL}/userProfile/${user.id}`).then((res) => {
				setCurrentUser(res.data);
			});
		}
	};

	const goDetail = (song) => {
		props.history.push(`/songpage/${song._id}`);
	};

	const getSongListFromOrigin = async (songmap) => {
		setOriginInfo(songmap)
		const filter = {
			filters: {
				origin: [songmap]
			}
		}
		setOriginProps(songmap)
		await Axios.post(`${BASE_URL}/list`, filter).then((res) => {
			setSongList(res.data.songs);
		});
		setSongOrigin(songmap);
	};

	const enterArea = (area) => {
		setHoveredArea(area);
	};

	const leaveArea = (area) => {
		setHoveredArea(null);
	};

	const getTipPosition = (area) => {
		return { top: '20em', right: '32em', position: 'absolute', color: 'black' };
	};

	const getPopularSong = () => {
		Axios.get(`${BASE_URL}/songList/popular`)
			.then(res => {
				setPopularSongs(res.data)
			})
	}

	const getNewUploadedCoverSong = () => {
		Axios.get(`${BASE_URL}/uploadSong/newUploaded`)
			.then(res => {
				setNewUploadedCoverSongs(res.data)
			})
	}

	const getNewSongs = () => {
		Axios.get(`${BASE_URL}/list/newUploaded`)
			.then(res => {
				setNewSongs(res.data)
			})
	}

	const handleSetGLobalCurrSong = (songData, source) => {
		localStorage.removeItem('songList');
		localStorage.setItem('currSong', JSON.stringify(songData))
		if (source === 'popular') {
			localStorage.setItem('songList', JSON.stringify(popularSongs))
		}
		else if (source === 'newCover') {
			localStorage.setItem('songList', JSON.stringify(newUploadedCoverSongs))
		}
		else if (source === 'newSong') {
			localStorage.setItem('songList', JSON.stringify(newSongs))
		}else{
			localStorage.setItem('songList', JSON.stringify(songList))
		}
		props.handleGetCurrSongGlobal()
		props.handleGetSongListGlobal()
	}

	const checkDataAvailability = () => {
		if(popularSongs.length > 0 && newSongs.length > 0){
			return true
		}
		return false
	}

	return (
		<div className="homepage">
			<div className="d-flex">
				<div className="body__songs w-100 mapper">
					<div>
						<SearchSong />
					</div>
					<center className='test-woi'>
						<ImageMapper
							src="https://suaratanahair.herokuapp.com/images/peta-indonesia.png"
							map={{
								name: 'Peta Indonesia',
								areas: [
									{ name: 'Aceh', coords: [1, 23, 40, 68], shape: 'rect' },
									{ name: 'Sumatera Utara', coords: [41, 39, 68, 89], shape: 'rect' },
									{ name: 'Riau', coords: [68, 64, 112, 89, 108, 104, 85, 107, 68, 88], shape: 'poly' },
									{
										name: 'Sumatera Barat',
										coords: [53, 90, 69, 89, 86, 108, 76, 120, 71, 136, 44, 109],
										shape: 'poly'
									},
									{
										name: 'Jambi',
										coords: [79, 118, 88, 108, 108, 105, 119, 108, 121, 115, 92, 130],
										shape: 'poly'
									},
									{ name: 'Bengkulu', coords: [81, 120, 75, 125, 107, 158, 114, 153], shape: 'poly' },
									{
										name: 'Sumatera Selatan',
										coords: [91, 130, 114, 153, 143, 135, 121, 116],
										shape: 'poly'
									},
									{
										name: 'Lampung',
										coords: [110, 157, 138, 139, 143, 165, 123, 171, 118, 164],
										shape: 'poly'
									},
									{
										name: 'Bangka Belitung',
										coords: [124, 116, 150, 139, 176, 135, 149, 108],
										shape: 'poly'
									},
									{ name: 'Banten', coords: [128, 179, 138, 168, 148, 168, 145, 182], shape: 'poly' },
									{
										name: 'Jawa Barat',
										coords: [149, 165, 144, 187, 173, 193, 174, 173],
										shape: 'poly'
									},
									{
										name: 'Jawa Tengah',
										coords: [174, 179, 199, 175, 211, 177, 203, 196, 195, 188, 188, 192, 175, 191],
										shape: 'poly'
									},
									{ name: 'Yogyakarta', coords: [188, 194, 195, 188, 203, 198], shape: 'poly' },
									{
										name: 'Jawa Timur',
										coords: [203, 197, 212, 176, 255, 178, 243, 198, 251, 205],
										shape: 'poly'
									},
									{ name: 'Bali', coords: [244, 197, 255, 193, 263, 199, 258, 211], shape: 'poly' },
									{ name: 'Nusa Tenggara Barat', coords: [263, 211, 306, 190], shape: 'rect' },
									{ name: 'Nusa Tenggara Timur', coords: [307, 233, 384, 188], shape: 'rect' },
									{
										name: 'Maluku',
										coords: [383, 128, 447, 127, 509, 167, 492, 213, 387, 212],
										shape: 'poly'
									},
									{
										name: 'Maluku Utara',
										coords: [427, 50, 364, 120, 392, 127, 426, 116],
										shape: 'poly'
									},
									{
										name: 'Sulawesi Utara',
										coords: [352, 81, 399, 31, 406, 43, 375, 88, 361, 91],
										shape: 'poly'
									},
									{ name: 'Gorontalo', coords: [334, 78, 331, 89, 361, 91, 352, 82], shape: 'poly' },
									{
										name: 'Sulawesi Tengah',
										coords: [333, 76, 330, 88, 367, 105, 348, 138, 325, 120, 308, 111, 318, 75],
										shape: 'poly'
									},
									{
										name: 'Sulawesi Barat',
										coords: [306, 109, 296, 141, 308, 138, 315, 115, 311, 112],
										shape: 'poly'
									},
									{
										name: 'Sulawesi Selatan',
										coords: [316, 114, 308, 138, 305, 161, 323, 175, 327, 131, 338, 130],
										shape: 'poly'
									},
									{
										name: 'Sulawesi Tenggara',
										coords: [329, 133, 326, 139, 341, 170, 365, 173, 370, 154, 335, 129],
										shape: 'poly'
									},
									{
										name: 'Kalimantan Selatan',
										coords: [249, 148, 273, 138, 274, 123, 263, 123, 259, 111, 243, 136],
										shape: 'poly'
									},
									{
										name: 'Kalimantan Tengah',
										coords: [
											261,
											108,
											253,
											85,
											238,
											86,
											230,
											100,
											201,
											113,
											201,
											131,
											213,
											138,
											243,
											137
										],
										shape: 'poly'
									},
									{
										name: 'Kalimantan Barat',
										coords: [
											176,
											68,
											175,
											94,
											188,
											114,
											192,
											131,
											200,
											133,
											200,
											113,
											229,
											99,
											243,
											74
										],
										shape: 'poly'
									},
									{
										name: 'Kalimantan Timur',
										coords: [
											237,
											87,
											243,
											73,
											263,
											40,
											291,
											42,
											294,
											75,
											303,
											83,
											284,
											103,
											273,
											123,
											263,
											123,
											261,
											108,
											253,
											85
										],
										shape: 'poly'
									},
									{
										name: 'Papua Barat',
										coords: [435, 121, 436, 92, 496, 103, 500, 127, 505, 140, 497, 157],
										shape: 'poly'
									},
									{
										name: 'Papua',
										coords: [503, 149, 506, 135, 498, 97, 578, 123, 578, 205, 537, 202],
										shape: 'poly'
									}
								]
							}}
							width={594}
							active={false }
							onClick={(area) => getSongListFromOrigin(area.name)}
							onMouseEnter={(area) => enterArea(area)}
							onMouseLeave={(area) => leaveArea(area)}
						/>

						{hoveredArea && (
							<p style={{fontWeight: '700', fontSize: '20px'}} className='font-family--helvetica' style={{ ...getTipPosition(hoveredArea) }}>{hoveredArea && hoveredArea.name}</p>
						)}
						<div className='mt-2 mb-4'></div>
					</center>
				</div>
			</div>
			<div className="">
				{
					songOrigin.length > 0
					&& <OriginSongPlaylist
						handleSetGLobalCurrSong={handleSetGLobalCurrSong}
						originSongs={songList}
						setColorRowIndex={props.setColorRowIndex}
						colorRowIndex={props.colorRowIndex}
						userId={currentUser.id ? currentUser.id : null}
						getSongListFromOrigin={getSongListFromOrigin}
						originProps={originProps}
						originInfo={originInfo}
					/>
				}
			</div>

			
				{	
					checkDataAvailability() ?
						<>
							<div>
								<PopularSongList
									handleSetGLobalCurrSong={handleSetGLobalCurrSong}
									popularSongs={popularSongs}
									setColorRowIndex={props.setColorRowIndex}
									colorRowIndex={props.colorRowIndex}
									userId={currentUser.id ? currentUser.id : null}
									setIsReportShow={setIsReportShow}
									getPopularSong={getPopularSong}
								/>
							</div>

							<div >
								<NewUploadedCoverSongList
									handleSetGLobalCurrSong={handleSetGLobalCurrSong}
									newUploadedCoverSongs={newUploadedCoverSongs}
									setColorRowIndex={props.setColorRowIndex}
									colorRowIndex={props.colorRowIndex}
									userId={currentUser.id ? currentUser.id : null}
									setIsReportShow={setIsReportShow}
									getNewUploadedCoverSong={getNewUploadedCoverSong}
								/>
							</div>
						<div>
							<NewSongList
								handleSetGLobalCurrSong={handleSetGLobalCurrSong}
								newSongs={newSongs}
								setColorRowIndex={props.setColorRowIndex}
								colorRowIndex={props.colorRowIndex}
								userId={currentUser.id ? currentUser.id : null}
								getNewSongs={getNewSongs}
							/>
						</div> 
					</> : <center> <Spinner variant='dark'  animation="border" /> </center>
				}
			 <div className="toast-notification position-toast">
				<Toast style={{color:'black'}} onClose={() => setIsReportShow(false)} show={isReportShow} delay={3000} autohide>
					<Toast.Body  >Anda berhasil melaporkan lagu!</Toast.Body>
				</Toast>
			</div>
			<div style={{ height: '100px' }}> </div>
		</div>
	);
};

export default HomePage;
