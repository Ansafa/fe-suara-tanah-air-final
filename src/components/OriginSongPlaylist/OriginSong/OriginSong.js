import React from 'react'
import Axios from 'axios';
import {FaPen, FaTrash} from 'react-icons/fa';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import FavoriteIcon from '@material-ui/icons/Favorite';
import './OriginSong.scss';
import { BASE_URL } from '../../Constant'

const OriginSong = ({
                      data, setCurrSong, index, setColorRowIndex, colorRowIndex,
                      userId, handleSetGLobalCurrSong,
                      goDetail, originProps, getSongListFromOrigin
                    }) => {

  const {_id: songId, likes, title, origin, writer} = data

  const newData = {
    ...data,
    index
  }

  const validateLike = () => {
    if (typeof data === 'object') {
      for (let i = 0; i < likes.length; i++) {
        if (likes[i] === userId) {
          return true;
        }
      }
      return false;
    }
  }

  const handleLike = async () => {
    const payload = {
      userId,
      songData: data
    }
    if (!validateLike()) {
      Axios.post(`${BASE_URL}/list/${songId}/likes/${userId}`, payload)
        .then(res => {
          getSongListFromOrigin(originProps)
        })
    } else {
      Axios.delete(`${BASE_URL}/list/${songId}/likes/${userId}`)
        .then(res => {
          getSongListFromOrigin(originProps)
        })
    }
  }

  const playSong = (newData, index) => {
    setColorRowIndex(index);
    setCurrSong(newData);
    handleSetGLobalCurrSong(newData)
  }

  return (
    <div>
      <hr className="customize-hr mt-1 mb-1 height-1px"/>
      <div
        className={`row songs ml-0 mr-0 ${colorRowIndex === index && index != undefined ? 'row-light-grey' : null}`}>
        <div onClick={() => playSong(newData, index)} className="col">
          <h6 className="title-songs  ml-02">
            {title}
          </h6>
        </div>
        <div className="col show">
          <h6 style={{color: 'white'}} className="custom-heading-songlist font-family--helvetica">{origin}</h6>
        </div>
        <div className="col show">
          <h6 style={{color: 'white'}} className="font-family--helvetica">{writer}</h6>
        </div>
        <div className="edit-icon col">
          <div className='row'>
            <div className='col'>
              {userId && <div style={{color: 'white'}} onClick={handleLike}>{!validateLike() ? <FavoriteBorderIcon/> :
                <FavoriteIcon/>}</div>}
            </div>
            <div className='col'>
              {
                <p onClick={() => goDetail(data)} className='go-detail'>Detail</p>
              }
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default OriginSong;
