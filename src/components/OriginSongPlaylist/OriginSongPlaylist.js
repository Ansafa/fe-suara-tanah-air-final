import React, { useState, useEffect } from "react";
import OriginSong from "./OriginSong/OriginSong";
import SongPlayer from "../AudioPlayer/AudioPlayer";
import { Modal, Button } from "react-bootstrap";
import "./OriginSongPlaylist.scss";
import Axios from "axios";
import Toast from 'react-bootstrap/Toast';
import { useHistory } from "react-router-dom";
import { BASE_URL } from '../Constant'

const OriginSongPlaylist = ({
  originSongs,
  setColorRowIndex,
  colorRowIndex,
  userId,
  handleSetGLobalCurrSong,
  setIsReportShow,
  getSongListFromOrigin,
  originProps,
  originInfo
}) => {
  const [currSong, setCurrSong] = useState({});
  const [currentUser, setCurrentUser] = useState({});
  const [songToBeDeleted, setSongToBeDeleted] = useState({});
  const [songToBeReported, setSongToBeReported] = useState({});
  const [isReportVisible, setIsReportVisible] = useState(false);

  const history = useHistory();

  const setReportedSong = (val, data) => {
    setIsReportVisible(val)
    setSongToBeReported(data)
  }

  const reportUploadSong = (data) => {
    const payload = {
      userId
    }
    Axios.post(`${BASE_URL}/uploadSong/${data._id}/report`, payload)
    .then(res => {
      setSongToBeReported({})
    })
    setIsReportShow(true)
    setIsReportVisible(false)
  }

   const goDetail = (song) => {
        localStorage.setItem('currSong', JSON.stringify(song))
        history.push(`/songpage/${song._id}`)
  }
  
  return (
    <div className="w-100">
      <div className="origin-song-wrapper">
        <center>
          {originInfo && <h4 style={{ color: "white", padding: "5px" }}>{`Lagu Daerah ${originInfo}`}</h4>}
        </center>
        <div className="row pl-5 pt-3">
          <div className="col">
            <h4 style={{ color: "white" }} className="custom-font-size-12">
              Judul Lagu
            </h4>
          </div>
          <div className="col show">
            <h4 style={{ color: "white" }} className=" custom-font-size-12">
              Asal Daerah
            </h4>
          </div>
          <div className="col new-year show">
            <h4 style={{ color: "white" }} className=" custom-font-size-12">
              Pencipta
            </h4>
          </div>
          <div className='col show'>
          </div>
        </div>
        {originSongs.map((song, i) => {
          return (
            <OriginSong
              data={song}
              userId={userId ? userId : null}
              setCurrSong={setCurrSong}
              index={i}
              colorRowIndex={colorRowIndex}
              setColorRowIndex={setColorRowIndex}
              setReportedSong={setReportedSong}
              handleSetGLobalCurrSong={handleSetGLobalCurrSong}
              getSongListFromOrigin={getSongListFromOrigin}
              originProps={originProps}
              goDetail={goDetail}
            />
          );
        })}
      </div>
      <div style={{height: '50px'}}> </div>
    </div>
  );
};

export default OriginSongPlaylist;
