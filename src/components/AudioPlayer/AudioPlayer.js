import React from 'react'
import AudioPlayer from 'react-h5-audio-player';
import 'react-h5-audio-player/lib/styles.css';

const SongPlayer = ({path, autoPlay, nextPrevBtn, nextSong, prevSong }) => {
    
    return (
        <div style={{ bottom: '0'}}>
                <AudioPlayer
                    style={{width: '100%', paddingRight: '35px'}}
                    src={path}
                    height="50px"
                    showSkipControls={nextPrevBtn ? true : false}
                    autoPlayAfterSrcChange={autoPlay ? true : false}
                    onClickNext={nextSong}
                    onClickPrevious={prevSong}
                />
        </div>
    )
}

export default SongPlayer
