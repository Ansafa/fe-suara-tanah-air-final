import React, { Component, useState, useEffect } from 'react';
import { FaPen, FaSearch } from "react-icons/fa";
import { Col, Row } from 'antd';
import { getCredential, removeCredential } from '../Utils';
import Axios from 'axios';
import Song from '../Song/Song';
import SongPlayer from '../AudioPlayer/AudioPlayer';
import CheckBox from './Section/Checkbox';
import { origin } from './Section/Datas'
import "./SongList.scss";
import SearchSong from '../SearchSong/SearchSong';
import Toast from 'react-bootstrap/Toast';
import Spinner from 'react-bootstrap/Spinner';
import { useHistory } from "react-router-dom";
import { BASE_URL } from '../Constant'

const SongList = (props) => {
    const [SongList, setSongList] = useState([])
    const [currSong, setCurrSong] = useState({})
    const [currentUser, setCurrentUser] = useState({})
    const [Filters, setFilters] = useState({ origin: [] })

    const [searchedSong, setSearchedSong] = useState('')
    const [ show, setShow ] = useState(false);

    const history = useHistory();

    const getSongsList = (variables) => {
        Axios.post(`${BASE_URL}/list`, variables)
            .then(res => {
                setSongList(res.data.songs)
            })
    }

    const setIndexColorRow = (id) => {
        props.setColorRowIndex(id);
    }

    const handleSetGLobalCurrSong = (songData) => {
        localStorage.removeItem('songList');
        localStorage.setItem('currSong', JSON.stringify(songData))
        localStorage.setItem('songList', JSON.stringify(SongList))
        props.handleGetCurrSongGlobal()
        props.handleGetSongListGlobal()
    }

    useEffect(() => {
        const variables = {}
        getSongsList(variables)
        getLoggedInUser();
        props.setCurrUrl('songList')
    }, []);

    const getLoggedInUser = () => {
        let user = JSON.parse(getCredential());
        if (user) {
            Axios.get(`${BASE_URL}/userProfile/${user.id}`)
                .then(res => {
                    setCurrentUser(res.data)
                })
        }
    }

    const goDetail = (song) => {
        localStorage.setItem('currSong', JSON.stringify(song))
        history.push(`/songpage/${song._id}`)
    }


    const showFilteredResults = (filters) => {
        const variables = {
            filters: filters
        }
        getSongsList(variables)
    }

    const handleFilters = (filters, category) => {
        const newFilters = { ...Filters }
        newFilters[category] = filters
        showFilteredResults(newFilters)
        setFilters(newFilters)
    }

    const handleSearchSong = (e) => {
       setSearchedSong(e.target.value)
    }

    return (
        <div className="w-100 tes">
            <div className="container break-container">
                <div className="row justify-content-between">
                    <div className="text-page-user">
                        <h4 className="mt-3 ml-5 font-family--helvetica text-black-custom">
                            <h5><strong>HALAMAN LAGU</strong></h5>
                        </h4>
                    </div>
                    <div className="search-song mr-5 mb-3 search-custom-song search-custom-song-2   ">
                        <FaSearch style={{ color: 'black'}} className='search-custom-song-icon'/>
                        <input
                          className="background-transparent border-black text-indent-10px mt-3 search-input"
                          placeholder="Cari lagu daerah"
                          type="text"
                          name="search"
                          onChange={handleSearchSong}
                        />
                    </div>
                </div>
                <div className="lagu-daerah background-brown">
                    <h6 className="ml-3" style={{ color: 'white' }}><strong>Filter lagu berdasarkan daerah</strong></h6>
                    <hr className="custom-hr-songlist" />
                    <Row>
                        <Col >
                            <CheckBox list={origin} handleFilters={(filters) => handleFilters(filters, 'origin')} />
                        </Col>
                    </Row>
                </div>
            </div>
            <div className="songlist-wrapper container px-5 mt-3 break-container">
                <div className="row pt-3 custom-padding-5">
                    <div className="col" style={{ paddingLeft: "3em" }}>
                        <h4 style={{ color: 'white' }} className="custom-font-size-12">Judul Lagu</h4>
                    </div>
                    <div className="col show">
                        <h4 style={{ color: 'white' }} className="custom-font-size-12">Asal Daerah</h4>
                    </div>
                    <div className="col pl-5 show new-year">
                        <h4 style={{ color: 'white' }} className="custom-font-size-12">Pencipta</h4>
                    </div>
                  <div className='col'>
                  </div>
                  <div className='col'>
                  </div>
                </div>
                <div className="toast-notification position-toast">
					<Toast onClose={() => setShow(false)} show={show} delay={3000} autohide>
						<Toast.Body>Anda berhasil unliked lagu!</Toast.Body>
					</Toast>
				</div>
                {
                    searchedSong ?
                       SongList.map((song, i) => {
                           let title = song.title.toLowerCase()
                                if(title.includes(searchedSong.toLowerCase())){
                                    return <Song
                                            data={song}
                                            goDetail={(song) => goDetail(song)}
                                            currSong={currSong}
                                            setCurrSong={setCurrSong}
                                            currentUser={currentUser}
                                            getSong={(variable) => getSongsList(variable)}
                                            // setIsPlayInstrumental={setIsPlayInstrumental}
                                            index={i}
                                            colorRowIndex={props.colorRowIndex}
                                            setColorRowIndex={props.setColorRowIndex}
                                            isSongList={true}
                                            handleSetGLobalCurrSong={handleSetGLobalCurrSong}
                                        />
                                }
                            })
                        :
                            SongList.map((song, i) => {
                                return <Song
                                    data={song}
                                    goDetail={(song) => goDetail(song)}
                                    currSong={currSong}
                                    setCurrSong={setCurrSong}
                                    currentUser={currentUser}
                                    getSong={(variable) => getSongsList(variable)}
                                    // setIsPlayInstrumental={setIsPlayInstrumental}
                                    setShow={setShow}
                                    index={i}
                                    colorRowIndex={props.colorRowIndex}
                                    setColorRowIndex={props.setColorRowIndex}
                                    isSongList={true}
                                    handleSetGLobalCurrSong={handleSetGLobalCurrSong}
                                />
                            })
                }

            </div>
               <center className='mt-5'>
                {
                    SongList.length === 0 && <Spinner  animation="border" />
                }
            </center>
            <div style={{height: '100px'}}> </div>
        </div>
    )

};

export default SongList;