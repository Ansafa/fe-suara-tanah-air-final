import React, { useState } from 'react'
import { Checkbox, Collapse } from 'antd';

const { Panel } = Collapse


const CheckBox = ({handleFilters, list})=> {

    const [Checked, setChecked] = useState([])

    const handleToggle = (value) => {
        const currentIndex = Checked.indexOf(value)
        const newChecked = [...Checked]
        if(currentIndex === -1){
            newChecked.push(value)
        }else{
            newChecked.splice(currentIndex, 1)
        }
        setChecked(newChecked)
        handleFilters(newChecked)
    }

    const renderCheckboxLists = () => list.map((value, index) => (
        <React.Fragment key={index} >
            <div style={{marginRight: '18px', color: 'white'}}>
                <Checkbox
                    style={{marginRight: '3px'}}
                    onChange={() => handleToggle(value.name)}
                    type="checkbox"
                    checked={Checked.indexOf(value.name) === -1 ? false : true}
                />
            {value.name}
           </div>
        </React.Fragment>
    ))
    

    return (
        <div>
            {/* <Collapse defaultActiveKey={['0']}>
                <Panel header="Daerah" key='1'> */}
                <div className="row margin-left-02">
                    {
                        renderCheckboxLists()
                    }
                </div>
                 {/* </Panel>
           </Collapse> */}
        </div>

    )
}

export default CheckBox
