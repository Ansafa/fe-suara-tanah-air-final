import React from 'react'
import Axios from 'axios';
import {FaPen, FaTrash} from 'react-icons/fa';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import FavoriteIcon from '@material-ui/icons/Favorite';
import { BASE_URL } from '../../Constant'

const UploadedSong = ({
                        data, setCurrSong, index, setColorRowIndex, colorRowIndex,
                        setDeleteUploadSong, userId, isSongPage, setReportedSong,
                        getSongsCoverUpload, handleSetGLobalCurrSong, handleSetGLobalCurrSongUserProfile,
                        isUserProfile
                      }) => {

  const {_id: songId, likes, songName, songCover, uploader} = data

  const newData = {
    ...data,
    index
  }

  const playSong = (newData, index) => {
    setColorRowIndex(index);
    setCurrSong(newData);
    if (isUserProfile) {
      handleSetGLobalCurrSongUserProfile(newData)
    } else {
      handleSetGLobalCurrSong(newData)
    }
  }

  const validateLike = () => {
    if (typeof data === 'object') {
      for (let i = 0; i < likes.length; i++) {
        if (likes[i] === userId) {
          return true;
        }
      }
      return false;
    }
  }

  const handleLike = async () => {
    const payload = {
      userId,
      songData: data
    }
    console.log(songId);
    console.log(userId);
    if (!validateLike()) {
      Axios.post(`${BASE_URL}/uploadSong/${songId}/likes/${userId}`, payload)
        .then(res => {
          console.log(res);
          getSongsCoverUpload(songCover._id)
        })
    } else {
      Axios.delete(`${BASE_URL}/uploadSong/${songId}/likes/${userId}`)
        .then(res => {
          getSongsCoverUpload(songCover._id)
        })
    }
  }

  return (
    <div>
      <hr className="customize-hr mt-1 mb-1 height-1px"/>
      <div
        className={`row uploadedSong-padding-first songs ml-0 mr-0  ${colorRowIndex === index && index != undefined ? 'row-light-grey' : null}`}>
        <div onClick={() => playSong(newData, index)} className="col">
          <h6 className="title-songs  ml-02">
            {songName}
          </h6>
        </div>
        <div className="col show">
          <h6 style={{color: 'white'}} className="font-family--helvetica">{songCover.origin}</h6>
        </div>
        <div className="col show">
          <h6 style={{color: 'white'}} className="font-family--helvetica">{uploader.username}</h6>
        </div>
        {
          (!isSongPage && (userId === uploader._id)) ? (
            <div className="edit-icon col" style={{textAlign: 'center'}}>
              <FaTrash className='' style={{color: 'white'}} onClick={() => setDeleteUploadSong(true, data)}/>
            </div>) : (
            userId &&
            <>
              <div className='col'>
                <div className='row'>
                  <div className="edit-icon col">
                    <div style={{color: 'white'}} onClick={handleLike}>{!validateLike() ? <FavoriteBorderIcon/> :
                      <FavoriteIcon/>}</div>
                  </div>
                  <div className='col'>
                    <p style={{color: 'white'}} onClick={() => setReportedSong(true, data)}> Report </p>
                  </div>
                </div>
              </div>
            </>
          )
        }
      </div>
    </div>
  )
 
}

export default UploadedSong
