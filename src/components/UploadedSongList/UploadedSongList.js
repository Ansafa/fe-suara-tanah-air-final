import React, { useState } from "react";
import UploadedSong from "./UploadedSong/UploadedSong";
import SongPlayer from "../AudioPlayer/AudioPlayer";
import { Modal, Button } from "react-bootstrap";
import "./UploadedSongList.scss";
import Axios from "axios";
import Toast from 'react-bootstrap/Toast';
import { BASE_URL } from '../Constant'

const UploadedSongList = ({
  uploadedSongs,
  getUserUploadedSong,
  userId,
  getSongsCoverUpload,
  isSongPage,
  songId,
  setIsShowDeleteUploadSong,
  setIsReportShow,
  handleGetCurrSongGlobal,
  handleGetSongListGlobal,
  setColorRowIndex,
  colorRowIndex,
  handleSetGLobalCurrSongUserProfile,
  isUserProfile
}) => {
  const [currSong, setCurrSong] = useState({});
  const [currentUser, setCurrentUser] = useState({});
  const [isVisible, setIsVisible] = useState(false);
  const [songToBeDeleted, setSongToBeDeleted] = useState({});
  const [songToBeReported, setSongToBeReported] = useState({});
  const [isReportVisible, setIsReportVisible] = useState(false);

  const setDeleteUploadSong = (val, data) => {
    setIsVisible(val);
    setSongToBeDeleted(data);
  };

  const deleteUploadedSong = (data) => {
    if(userId === data.uploader._id){
      Axios.delete(`${BASE_URL}/song/uploadFile/${data._id}`).then((res) => {
        setSongToBeDeleted({});
      });
      setIsVisible(false);
      setIsShowDeleteUploadSong(true)
      if (isSongPage) {
        getSongsCoverUpload(songId);
      } else {
        getUserUploadedSong(userId);
      }
    }
  };

  const setReportedSong = (val, data) => {
    setIsReportVisible(val)
    setSongToBeReported(data)
  }

  const reportUploadSong = (data) => {
    const payload = {
      userId
    }
    Axios.post(`${BASE_URL}/uploadSong/${data._id}/report`, payload)
    .then(res => {
      setSongToBeReported({})
    })
    setIsReportShow(true)
    setIsReportVisible(false)
  }

  const handleSetGLobalCurrSong = (songData) => {
        localStorage.removeItem('songList');
        localStorage.setItem('currSong', JSON.stringify(songData))
        localStorage.setItem('songList', JSON.stringify(uploadedSongs))
        handleGetCurrSongGlobal()
        handleGetSongListGlobal()
    }

  return (
    <div className="w-100">
      <div className="uploaded-song-wrapper">
        <center>
          <h4 style={{ color: "white", padding: "5px" }}>{isUserProfile ? 'Lagu Cover Anda' : 'Lagu Cover User'}</h4>
        </center>
        <div className="row pt-3 uploadedSong-heading-padding">
          <div className="col">
            <h4 style={{ color: "white" }} className="custom-font-size-12 ">
              Judul Lagu
            </h4>
          </div>
          <div className="col show">
            <h4 style={{ color: "white" }} className="custom-font-size-12 ">
              Asal Daerah
            </h4>
          </div>
          <div className="col show new-year">
            <h4 style={{ color: "white" }} className="custom-font-size-12">
              Uploader
            </h4>
          </div>
          <div className='col show'>
          </div>
        </div>
        {uploadedSongs.map((song, i) => {
          return (
            <UploadedSong
              data={song}
              userId={userId ? userId : null}
              setCurrSong={setCurrSong}
              index={i}
              colorRowIndex={colorRowIndex}
              setColorRowIndex={setColorRowIndex}
              setDeleteUploadSong={setDeleteUploadSong}
              isSongPage={isSongPage}
              setReportedSong={setReportedSong}
              getSongsCoverUpload={getSongsCoverUpload}
              handleSetGLobalCurrSong={handleSetGLobalCurrSong}
              handleSetGLobalCurrSongUserProfile={handleSetGLobalCurrSongUserProfile}
              isUserProfile={isUserProfile}
            />
          );
        })}
      </div>
      <Modal show={isVisible} onHide={() => setIsVisible(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Hapus Lagu</Modal.Title>
        </Modal.Header>
        <Modal.Body>{`Anda yakin ingin menghapus lagu ${songToBeDeleted.songName}?`}</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setIsVisible(false)}>
            Tutup
          </Button>
          <Button
            variant="danger"
            onClick={() => deleteUploadedSong(songToBeDeleted)}
          >
            Hapus Lagu
          </Button>
        </Modal.Footer>
      </Modal>
      <Modal show={isReportVisible} onHide={() => setIsReportVisible(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Laporkan Lagu</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {
            `Apakah Anda yakin lagu ${songToBeReported.songName} tidak sesuai judul / mengandung SARA`
          }
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setIsReportVisible(false)}>
            Tutup
          </Button>
          <Button
            variant="danger"
            onClick={() => reportUploadSong(songToBeReported)}
          >
            Iya, Laporkan Lagu
          </Button>
        </Modal.Footer>
      </Modal>
      <div style={{height: '150px'}}> </div>
    </div>
  );
};

export default UploadedSongList;
