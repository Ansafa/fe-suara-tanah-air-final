import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import { Redirect } from "react-router-dom";
import { FaPen, FaSearch } from 'react-icons/fa';
import './SearchSong.scss'
import { BASE_URL } from '../Constant'

const SearchSong = () => {
    const [searchSongListDefault, setSearchSongListDefault] = useState();
    const [searchSongList, setSearchSongList] = useState([]);
    const [searchText, setSearchText] = useState();
    const [redirect, setRedirect] = useState(null);
    const [url, setUrl] = useState();

    const fetchData = (variables) => {
        Axios.post(`${BASE_URL}/list`, variables)
            .then(res => {
                setSearchSongList(res.data.songs)
                setSearchSongListDefault(res.data.songs)
            })
    }

    const updateInput = (e) => {
        setSearchText(e.target.value);
        let filtered = searchSongListDefault.filter(song => {
            return song.title.toLowerCase().includes(e.target.value.toLowerCase())
        })
        setSearchSongList(filtered);
    }

    const clickMe= (song) => {
        setUrl(`/songpage/${song._id}`)
        setRedirect(true);
    }

    useEffect(() => {
        const variables = {};
        fetchData(variables);
    }, []);

    return (
        <>
            <div>
                <div className="search-song mr-5 mb-3 search-custom-song">
                    <FaSearch style={{ color: 'black'}} className='search-custom-song-icon'/>
                    <input
                        className="background-transparent border-black text-indent-10px mt-3 search-input"
                        placeholder="Cari lagu daerah"
                        type="text"
                        name="search"
                        onChange={updateInput}
                    />
                </div>
            </div>
            <div className='custom-searchSong'>
            {
                searchText
                && searchSongList.map((data) => {
                    if (data) {
                        return (
                            <div >
                                <li className='list-searchSong' onClick={() => clickMe(data)}>{data.title}</li>
                            </div>
                        )
                    }
                })
            }
            {
                 (searchText && searchSongList.length === 0) &&
                           (  
                                <div >
                                    <li className='list-searchSong'>Lagu tidak ada</li>
                                </div>
                           )
            }
            </div>
            {
                redirect
                && <Redirect to={url} />
            }
        </>
    );
}

export default SearchSong;
