import React, {useEffect, useState} from "react";
import "./Body.css";
import PlayCircleFilledIcon from "@material-ui/icons/PlayCircleFilled";
import FavoriteIcon from "@material-ui/icons/Favorite";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import Axios from "axios";

const Body = () => {
  const [audioPath, setAudioPath] = useState('')

  const getSong = () => {
   
  }

  useEffect(() => {
    // getSong();
    audioTune.load();
  }, [audioPath])

   const audioTune = new Audio(audioPath);

  // play audio sound
  const playSound = () => {
    Axios.get(`/songpage/5f7db65e31c3743d1323f913`).then( res => {
      setAudioPath(res.data.path)
    }) 
    audioTune.play();
  }
 
  // pause audio sound
  const pauseSound = () => {
    audioTune.pause();
  }
 
  // stop audio sound
  const stopSound = () => {
    audioTune.pause();
    audioTune.currentTime = 0;
  }

  return (
    <div className="body">
      <div className="body__info">
        <div className="body__infoText">
          <strong>PLAYLIST</strong>
  
          <h2>Discover Weeklyss</h2>
                 
          {/* <p>{discover_weekly?.description}</p> */}
        </div>
      </div>

      <div className="body__songs">
        <div className="body__icons">
          <PlayCircleFilledIcon
            className="body__shuffle"
          // onClick={playPlaylist}
          />
          <FavoriteIcon fontSize="large" />
          <MoreHorizIcon />
        </div>

          <input style={{color: 'white'}} type="button" className="btn btn-primary mr-2" value="Play" onClick={playSound}>Play</input>
          <input type="button" className="btn btn-warning mr-2" value="Pause" onClick={pauseSound}>pause</input>
          <input type="button" className="btn btn-danger mr-2" value="Stop" onClick={stopSound}>stop</input>
 

      </div>
    </div>
  );
}

export default Body;
