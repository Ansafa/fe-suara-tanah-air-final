import React, { useState } from "react";
import NewUploadedCoverSong from "./NewUploadedCoverSong/NewUploadedCoverSong";
import SongPlayer from "../AudioPlayer/AudioPlayer";
import { Modal, Button } from "react-bootstrap";
import "./NewUploadedCoverSongList.scss";
import Axios from "axios";
import Toast from 'react-bootstrap/Toast';
import { BASE_URL } from '../Constant'


const NewUploadedCoverSongList = ({
  newUploadedCoverSongs,
  setColorRowIndex,
  colorRowIndex,
  userId,
  handleSetGLobalCurrSong,
  getNewUploadedCoverSong,
  setIsReportShow
}) => {
  const [currSong, setCurrSong] = useState({});
  const [currentUser, setCurrentUser] = useState({});
  const [songToBeDeleted, setSongToBeDeleted] = useState({});
  const [songToBeReported, setSongToBeReported] = useState({});
  const [isReportVisible, setIsReportVisible] = useState(false);
  // const [isReportShow, setIsReportShow] = useState(false)

  const setReportedSong = (val, data) => {
    setIsReportVisible(val)
    setSongToBeReported(data)
  }

  const reportUploadSong = (data) => {
    const payload = {
      userId
    }
    Axios.post(`${BASE_URL}/uploadSong/${data._id}/report`, payload)
    .then(res => {
      setSongToBeReported({})
    })
    setIsReportShow(true)
    setIsReportVisible(false)
  }

  return (
    <div className="w-100">
      <div className="popular-song-wrapper">
        <center>
          <h4 style={{ color: "white", padding: "5px" }}>Lagu Cover Terbaru</h4>
        </center>
        <div className="row pt-3">
          <div className="col pl-5">
            <h4 style={{ color: "white" }} className="custom-font-size-12">
              Judul Lagu
            </h4>
          </div>
          <div className="col show">
            <h4 style={{ color: "white" }} className="custom-font-size-12">
              Asal Daerah
            </h4>
          </div>
          <div className="col new-year show">
            <h4 style={{ color: "white" }} className="custom-font-size-12">
              Uploader
            </h4>
          </div>
          <div className='col show'>
          </div>
        </div>
        {newUploadedCoverSongs.map((song, i) => {
          return (
            <NewUploadedCoverSong
              data={song}
              userId={userId ? userId : null}
              setCurrSong={setCurrSong}
              index={i}
              colorRowIndex={colorRowIndex}
              setColorRowIndex={setColorRowIndex}
              setReportedSong={setReportedSong}
              handleSetGLobalCurrSong={handleSetGLobalCurrSong}
              getNewUploadedCoverSong={getNewUploadedCoverSong}
            />
          );
        })}
      </div>
      <Modal show={isReportVisible} onHide={() => setIsReportVisible(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Laporkan Lagu</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {
            `Apakah Anda yakin lagu ${songToBeReported.songName} tidak sesuai judul / mengandung SARA`
          }
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setIsReportVisible(false)}>
            Tutup
          </Button>
          <Button
            variant="danger"
            onClick={() => reportUploadSong(songToBeReported)}
          >
            Iya, Laporkan Lagu
          </Button>
        </Modal.Footer>
      </Modal>  {/* <div className="toast-notification position-toast">
				<Toast onClose={() => setIsReportShow(false)} show={isReportShow} delay={3000} autohide>
					<Toast.Body>Anda berhasil melaporkan lagu!</Toast.Body>
				</Toast>
			</div>
      */}
      <div style={{height: '80px'}}> </div>
    </div>
  );
};

export default NewUploadedCoverSongList;
