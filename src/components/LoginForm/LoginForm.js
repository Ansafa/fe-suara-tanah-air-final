import React from 'react';
import Axios from 'axios';
import { BASE_URL } from '../Constant';
import { Redirect } from 'react-router-dom';
import { getCredential, setCredential } from '../Utils';
import './login.scss';
import MountainImage from '../../public_images/mountain.png';


class LoginForm extends React.Component {
    state = {
        redirect: false,
        url: '',
        currentUser: {},
        email: '',
        password: ''
    }

    _handleUser = (event) => {
        const email = event.target.value;
        this.setState({
            email: email
        });
    }
    //testing
    _handlePassword = (event) => {
        const password = event.target.value;
        this.setState({
            password: password
        });
    }

    async _loginUser() {
        Axios.post(`${BASE_URL}/login`, { email: this.state.email, password: this.state.password })
            .then((response) => {
                console.log(response)
                if (response.data.msg === 'user fetched') {
                    this.setState(
                        { currentUser: response.data.user[0], url: `/`, redirect: true }, 
                        () => setCredential(this.state.currentUser));
                            window.location.reload(true);

                }
                else {
                    this.setState({
                        errorMsg: response.data.msg
                    })
                }
            })
      

    }

    async _registerUser() {
        await this.setState({ redirect: true, url: '/register' });
        window.location.reload(false);
    }

    render() {
        const isRedirect = this.state.redirect;
        const url = this.state.url;

        return (
            isRedirect ? <Redirect to={url} /> :
            <div className="ml-5 mr-0 max-width-100">
                <div className="row custom-height-100vh">
                    <div className="col-md-6 pt-5 mt-5">
                    <form>
                        <div className="row font-family--helvetica">
                            <label className="mb-3 ml-3"><strong>Email</strong></label>
                        </div>
                        <div className="row">
                            <input className="custom-textbox-login custom-login-input search-input" type="text" placeholder="Masukkan Email" onChange={this._handleUser} />
                        </div>
                        <div className="row font-family--helvetica mt-5">
                            <label className="mb-3 ml-3"><strong>Password</strong></label>
                        </div>
                        <div className="row">
                            <input className="custom-textbox-login custom-login-input search-input" type="password" placeholder="Masukkan Password" onChange={this._handlePassword} />
                        </div>
                        <div className="mt-3 pt-5 mb-2 pb-5 custom-login--btn__container">
                            <button className="btn btn-primary custom-login--btn font-family--helvetica" type="button" onClick={() => this._loginUser()}>Masuk</button>
                        </div>
                        {this.state.errorMsg !== '' ?  <div className="row mt-5 custom-width-75">
                           <h4 className="text-danger">{this.state.errorMsg}</h4>
                        </div> : null }

                        <div className="row mt-5 custom-width-75">
                            <button className="background-color--grey--opacity100 text-black-custom font-family--helvetica border-none border-radius-10px custom-register--btn" type="button" onClick={() => this._registerUser()}>Daftar</button>
                        </div>

                        
                    </form>
                    </div>
                    <div className="col-md-6 login__image--container">
                        <img className='login__image' src={MountainImage}/>
                    </div>
                </div>
            </div>

        )
    }
}

export default LoginForm
