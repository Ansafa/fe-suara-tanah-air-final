import React, { useState } from "react";
import NewSong from "./NewSong/NewSong";
import SongPlayer from "../AudioPlayer/AudioPlayer";
import { Modal, Button } from "react-bootstrap";
import "./NewSongList.scss";
import Axios from "axios";
import Toast from 'react-bootstrap/Toast';
import { useHistory } from "react-router-dom";
import { BASE_URL } from '../Constant'


const NewSongList = ({
  newSongs,
  setColorRowIndex,
  colorRowIndex,
  userId,
  handleSetGLobalCurrSong,
  setIsReportShow,
  getNewSongs
}) => {
  const [currSong, setCurrSong] = useState({});
  const [currentUser, setCurrentUser] = useState({});

  const history = useHistory();

  const goDetail = (song) => {
        localStorage.setItem('currSong', JSON.stringify(song))
        history.push(`/songpage/${song._id}`)
  }



  return (
    <div className="w-100">
      <div className="popular-song-wrapper">
        <center>
          <h4 style={{ color: "white", padding: "5px" }}>Lagu Terbaru</h4>
        </center>
        <div className="row pt-3">
          <div className="col pl-5" >
            <h4 style={{ color: "white" }} className="custom-font-size-12">
              Judul Lagu
            </h4>
          </div>
          <div className="col show">
            <h4 style={{ color: "white" }} className="custom-font-size-12">
              Asal Daerah
            </h4>
          </div>
          <div className="col new-year show">
            <h4 style={{ color: "white" }} className="custom-font-size-12">
              Pencipta
            </h4>
          </div>
          <div className='col show'>
          </div>
        </div>
        {newSongs.map((song, i) => {
          return (
            <NewSong
              data={song}
              userId={userId ? userId : null}
              setCurrSong={setCurrSong}
              index={i}
              colorRowIndex={colorRowIndex}
              setColorRowIndex={setColorRowIndex}
              handleSetGLobalCurrSong={handleSetGLobalCurrSong}
              getNewSongs={getNewSongs}
              goDetail={goDetail}
            />
          );
        })}
      </div>
      <div style={{height: '80px'}}> </div>
    </div>
  );
};

export default NewSongList;
