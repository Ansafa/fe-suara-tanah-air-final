import React from 'react'

const AdminSong = ({
  data, deleteSong, setIsVisible, setDeleteSong, 
  handleSetGLobalCurrSong, index, colorRowIndex, setColorRowIndex
}) => {

  const handleSetSong = (data) => {
        handleSetGLobalCurrSong(data)
    }

    const newData = { 
        ...data,
        index
     }



  return (
    <div>
      <hr className="customize-hr mt-1 mb-1 height-1px"/>
      <div  onClick={() => setColorRowIndex(index)} className={`row songs ml-0 mr-0  ${colorRowIndex === index && index != undefined ? 'row-light-grey' : null}`} >
        <div className="col">
          <h6 className="title-songs  ml-02 custom-padding-2" onClick={() => handleSetGLobalCurrSong(newData)}>
            {data.songName}
          </h6>
        </div>
        <div className="col show">
          <h6 style={{color: 'white'}}
              className="font-family--helvetica word-wrap AdminSong-heading">{data.songCover.title}</h6>
        </div>
        <div className="col show">
          <h6 style={{color: 'white'}}
              className="font-family--helvetica word-wrap AdminSong-heading">{data.uploader.email}</h6>
        </div>
        <div className='col'>
          <div className='row'>
            <div className='col'>
              <h6 style={{color: 'white'}}
                  className="font-family--helvetica center-report AdminSong-heading">{data.reports.length}</h6>
            </div>
            <div className='col'>
              <button className="btn btn-danger deleteBtn-admin" onClick={() => setDeleteSong(true, data)}
                      type="button">Delete
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default AdminSong
