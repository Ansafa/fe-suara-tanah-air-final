import React, {useEffect, useState} from 'react'
import Axios from 'axios'
import AdminSong from './AdminSong';
import {getCredential, removeCredential, setCredential} from '../Utils';
import {Modal, Button, Container} from 'react-bootstrap';
import './AdminPage.scss'
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { BASE_URL } from '../Constant'
import Toast from 'react-bootstrap/Toast';

const AdminPage = ({setCurrUrl, handleGetCurrSongGlobal, handleGetSongListGlobal, setColorRowIndex, colorRowIndex}) => {
    const [songsUploadList, setSongsUploadList] = useState([])
    const [currentUser, setCurrentUser] = useState({})
    const [songsUploadWithReport, setSongsUploadWithReport] = useState([])
    const [ isVisible, setIsVisible ] = useState(false);
    const [songToBeDeleted, setSongToBeDeleted] = useState({})
    const [isAscending, setIsAscending] = useState(false)
      const [isShowDeleteUploadSong, setIsShowDeleteUploadSong] = useState(false)

  const {isAdmin} = currentUser;

    const getLoggedInUser = async () => {
        let user = JSON.parse(getCredential());
        if (user) {
          console.log('masuk');
            Axios.get(`${BASE_URL}/userProfile/${user.id}`)
            .then(res => {
                console.log(res);
                setCurrentUser(res.data)
            })
        }
    }

    useEffect(() => {
        getLoggedInUser()
        getSongsUpload()
        getSongsUploadWithLimitedReports()
        setCurrUrl('admin')
    }, [])
    
    const getSongsUpload = () => {
        Axios.get(`${BASE_URL}/admin/uploadSongs`)
        .then(res => {
            setSongsUploadList(res.data.songs)
        })
    }

    const getSongsUploadWithLimitedReports = () => {
        Axios.get(`${BASE_URL}/admin/uploadSongs/reports`)
        .then(res => {
            setSongsUploadWithReport(res.data)
        })
    }

    const toggleSort = () => {
        const sorted = songsUploadWithReport.sort((a, b) => {
            if(isAscending){
                return a.reports.length - b.reports.length
            }
            return b.reports.length - a.reports.length
        })
        setIsAscending(!isAscending)
        setSongsUploadWithReport(sorted)
    }

    const deleteSong = (data) => {
        Axios.delete(`${BASE_URL}/admin/uploadSongs/${data._id}`)
        .then(res => {
            console.log(res.data)
            getSongsUpload()
            if(songsUploadWithReport.length > 0 ){
                getSongsUploadWithLimitedReports()
            }
            setIsVisible(false)
            setIsShowDeleteUploadSong(true)
        })
    }

    const setDeleteSong = (val, data) => {
        setIsVisible(val)
        console.log(data);
        setSongToBeDeleted(data)
    }

    const handleSetGLobalCurrSong = (songData) => {
        localStorage.removeItem('songList');
        localStorage.setItem('currSong', JSON.stringify(songData))
        localStorage.setItem('songList', JSON.stringify(songsUploadList))
        handleGetCurrSongGlobal()
        handleGetSongListGlobal()
    }


  return (
    <div style={{width: '100%'}} className='admin-page'>
      {
        isAdmin ?
          <>
            {
              songsUploadWithReport.length > 0 &&
              <div className="AdminSong-wrapper">
                <center><h4 style={{color: 'white', padding: '5px'}}>Lagu Dengan Report</h4></center>
                <div className="row pt-3">
                  <div className="col">
                    <h4 style={{color: 'white'}} className="custom-font-size-12 custom-padding">Judul</h4>
                  </div>
                  <div className="col show">
                    <h4 style={{color: 'white'}} className="custom-font-size-12">Lagu Cover</h4>
                  </div>
                  <div className="col show new-year">
                    <h4 style={{color: 'white'}} className="custom-font-size-12">Uploader</h4>
                  </div>
                  <div className="col">
                    <h4 style={{color: 'white'}} className="custom-font-size-12 custom-padding-3">Report</h4>
                    <span style={{color: 'white', cursor: 'pointer'}} className='custom-padding-4' onClick={toggleSort}>{isAscending ? <ExpandMoreIcon /> : <ExpandLessIcon /> }</span>
                  </div>
                </div>
                {
                  songsUploadWithReport.map((song, i) => {
                    return <AdminSong
                      data={song}
                      index={i}
                      currentUser={currentUser}
                      setDeleteSong={setDeleteSong}
                      handleSetGLobalCurrSong={handleSetGLobalCurrSong}
                      colorRowIndex={colorRowIndex}
                      setColorRowIndex={setColorRowIndex}
                    />
                  })
                }
              </div>
            }
            <div className="AdminSong-wrapper">
                <div className="row pt-3">
                  <div className="col">
                    <h4 style={{color: 'white'}} className="custom-font-size-12 custom-padding">Judul</h4>
                  </div>
                  <div className="col show">
                    <h4 style={{color: 'white'}} className="custom-font-size-12 2">Lagu Cover</h4>
                  </div>
                  <div className="col show new-year">
                    <h4 style={{color: 'white'}} className="custom-font-size-12">Uploader</h4>
                  </div>
                  <div className="col">
                    <h4 style={{color: 'white'}} className="custom-font-size-12 custom-padding-3 ">Report</h4>

                  </div>
                </div>
              {
                songsUploadList.map((song, i) => {
                  return <AdminSong
                    data={song}
                    index={i}
                    currentUser={currentUser}
                    deleteSong={deleteSong}
                    setDeleteSong={setDeleteSong}
                    handleSetGLobalCurrSong={handleSetGLobalCurrSong}
                    colorRowIndex={colorRowIndex}
                    setColorRowIndex={setColorRowIndex}
                  />
                })
              }
              <Modal show={isVisible} onHide={() => setIsVisible(false)}>
                <Modal.Header closeButton>
                  <Modal.Title>Hapus Lagu</Modal.Title>
                </Modal.Header>
                <Modal.Body>{`Anda yakin ingin menghapus lagu ${songToBeDeleted.songName} ini?`}</Modal.Body>
                <Modal.Footer>
                  <Button variant="secondary" onClick={() => setIsVisible(false)}>
                    Tutup
                  </Button>
                  <Button variant="danger" onClick={() => deleteSong(songToBeDeleted)}>
                    Hapus Lagu
                  </Button>
                </Modal.Footer>
              </Modal>
               <div className="toast-notification position-toast">
                  <Toast style={{color: 'black'}} onClose={() => setIsShowDeleteUploadSong(false)} show={isShowDeleteUploadSong} delay={3000} autohide>
                    <Toast.Body>Anda berhasil menghapus lagu user!</Toast.Body>
                  </Toast>
              </div>
            </div>
          </>
          :
          <div className='forbidden-access font-family--helvetica '>
            <center><p>Anda tidak memiliki akses ke halaman ini </p></center>
          </div>
      }
      <div style={{height: '200px'}}> </div>
    </div>
  )
}

export default AdminPage
