import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import { BASE_URL } from '../Constant'
import Toast from 'react-bootstrap/Toast';

const UploadImage = ({playlistId, userId, setIsEditImg, getPlaylist, setIsImageToast}) => {
    const [selectedFile, setSelectedFile] = useState({});
    const [typeImage, setTypeImage] = useState({})
    const [show, setShow] = useState(false)

    const fileData = () => {

        if (selectedFile) {
            setTypeImage(selectedFile.type.substring(0,6));
            if (typeImage === "image/") {
                return (
                    <div>
                        <h2>File Details:</h2>
                        <p>File Name: {selectedFile.name}</p>
                        <p>File Type: {selectedFile.type}</p>
                    </div>
                );
            } else {
                return (
                    <div>
                        <h2>Please upload file with image format</h2>
                    </div>
                )
            }
        }
    };

    const onFileChange = (e) => {
        setSelectedFile(e.target.files[0]);
    };

    const onFileUpload = async () => {
        console.log('a');
        setTypeImage(selectedFile.type.substring(0,6));
        if (selectedFile && typeImage === "image/") {
            console.log('b');
            const formData = new FormData();
            formData.append(
                "file",
                selectedFile,
                selectedFile.name
            );

            await Axios.post(`${BASE_URL}/playlist/uploadFile/data`, { playlistId, imageName: selectedFile.name }).then(res => {
                console.log(res.data);
            });

            await Axios.post(`${BASE_URL}/playlist/uploadfile`, formData).then(res => {
                console.log(res);
            });
            // await redirectAfterUpload()
            await getPlaylist(playlistId)
            setIsImageToast(true)
            setIsEditImg(false)
        }else{
            setShow(true)
        }
    };

     const redirectAfterUpload = () => {
         const newUrl = `/profile/${userId}/playlist/${playlistId}`;
         window.location.href = newUrl;
    }

    const handleSubmitCoverPlaylist = async () => {
       await onFileUpload()
       
    }

    return (
        <div className='row'>
          <h6 className='mt-2'>Cover Playlist</h6>
            <div className="col-sm-3 mr-5">
                <label className="custom-file-upload m-0">
                    <div className="btn btn-secondary">Choose File</div>
                    <input type="file" className="upload-song-input" onChange={onFileChange} />
                </label>
            </div>
            <div className='col-sm-3'> <p>{selectedFile.name ? selectedFile.name : 'Choose a file..' }</p> </div>
             <div className="col-sm-2">
                    <button className="btn btn-primary height-100" onClick={handleSubmitCoverPlaylist}>
                        Upload!
                    </button>
             </div>
             <div className="toast-notification position-toast">
					<Toast onClose={() => setShow(false)} show={show} delay={3000} autohide>
						<Toast.Body>Mohon untuk memasukan file dengan format image</Toast.Body>
					</Toast>
			</div>
        </div>
    )
};

export default UploadImage;
