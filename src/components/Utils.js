export const setCredential = (token) => {
    return localStorage.setItem('user', JSON.stringify(token));
}

export const getCredential = () => {
    return localStorage.getItem('user');
}

export const removeCredential = () => {
    localStorage.removeItem('user');
}
