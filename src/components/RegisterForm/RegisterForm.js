import React from 'react';
import { BASE_URL } from '../Constant';
import { Redirect } from 'react-router-dom';
import Axios from 'axios';
import '../LoginForm/login.scss';
import MountainImage from "../../public_images/mountain.png";
class RegisterForm extends React.Component {
    state = {
        redirect: false,
        url: '',
        currentUser: {},
        username: '',
        password: '',
        confirmPassword: '',
        credential: '',
        email: '',
        emailFormatValidation: '',
        emailExistValidation: '',
        passwordValidation: '',
        formValidation: ''
    }

    _handleUser = (event) => {
        const username = event.target.value;
        this.setState({
            username: username
        });
    }

    _handlePassword = (event) => {
        const password = event.target.value;
        this.setState({
            password: password
        });
    }

     _handleConfirmPassword = (event) => {
        this.setState({
            passwordValidation: ''
        });
        const confirmPassword = event.target.value;
        this.setState({
            confirmPassword: confirmPassword
        });
    }

    _handleValidatePassword = () => {
        if (this.state.password === this.state.confirmPassword) {
            this.setState({
                passwordValidation: ''
            });
        } else {
            this.setState({
                passwordValidation: "Konfirmasi untuk password anda salah"
            });
        }
    }

    _handleEmail = (event) => {
        this.setState({
            emailExistValidation: ''
        });
        const email = event.target.value;
        const re = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (!re.test(String(email).toLowerCase())) {
            this.setState({
                emailFormatValidation: "Masukkan alamat Email anda dengan benar"
            });
        } else {
            this.setState({
                emailFormatValidation: ''
            });
            this.setState({
                email: email
            });
        }
    };

    _clearValidation = () => {
        this.setState({
            emailExistValidation: ''
        });
        this.setState({
            formValidation: ''
        });
    }


      async _register() {
        this._clearValidation();
        this._handleValidatePassword();
        await Axios.post(`${BASE_URL}/users`, { username: this.state.username, password: this.state.password, email: this.state.email })
            .then(res => {
                if (res.data.errorMessage === "Email sudah terdaftar") {
                    this.setState({
                        emailExistValidation: res.data.errorMessage
                    });
                } else if (res.data.errorMessage === "Mohon untuk mengisi semua field yang telah disediakan") {
                    this.setState({
                        formValidation: res.data.errorMessage
                    });
                } else {
                    if (this.state.emailFormatValidation === '' && this.state.passwordValidation === '') {
                        this.setState( 
                            { currentUser: res.data, url: `/login`, redirect: true });
                        window.location.reload(false);
                    }
                }
            });
    }

    render() {
        const isRedirect = this.state.redirect;
        const url = this.state.url;
        return (
            isRedirect ? <Redirect to={url} /> :
                <div className="mr-0 max-width-100 ml-5">
                    <div className="row custom-height-100vh">
                        <div className="col-md-6">
                            <form>
                                <div className="row font-family--helvetica mt-5">
                                    <label className="mb-3  ml-3"><strong>Username</strong></label>
                                </div>
                                <div className="row">
                                    <input className="custom-textbox-login custom-login-input search-input" type="text" placeholder="Masukkan username" onChange={this._handleUser} />
                                </div>
                                <div className="row font-family--helvetica mt-5">
                                    <label className="mb-3 ml-3"><strong>Email</strong></label>
                                </div>
                                <div className="row">
                                    <input className="custom-textbox-login custom-login-input search-input" type="email" placeholder="Masukkan Email" onChange={this._handleEmail} />
                                </div>
                                <div style={{color: 'red'}}>
                                    {
                                        (this.state.emailFormatValidation !== '') &&
                                        <div>
                                            {this.state.emailFormatValidation}
                                        </div>
                                    }
                                    {
                                        (this.state.emailExistValidation !== '') &&
                                        <div>
                                            {this.state.emailExistValidation}
                                        </div>
                                    }
                                </div>
                                <div className="row font-family--helvetica mt-5">
                                    <label className="mb-3 ml-3"><strong>Password</strong></label>
                                </div>
                                <div className="row">
                                    <input className="custom-textbox-login custom-login-input search-input" type="password" placeholder="Masukkan Password" onChange={this._handlePassword} />
                                </div>
                                <div className="row font-family--helvetica mt-5">
                                    <label className="mb-3 ml-3"><strong>Confirm Password</strong></label>
                                </div>
                                <div className="row">
                                    <input className="custom-textbox-login custom-login-input search-input" type="password" placeholder="Masukan Confirm Password" onChange={this._handleConfirmPassword}/>
                                </div>
                                <div style={{color: 'red'}}>
                                      {
                                        (this.state.passwordValidation !== '') &&
                                        <div>
                                            {this.state.passwordValidation}
                                        </div>
                                    }
                                </div>
                                <div style={{color: 'red'}}>
                                    {
                                        (this.state.formValidation !== '') &&
                                        <div>
                                            {this.state.formValidation}
                                        </div>
                                    }
                                </div>
                                <div className="row mt-5 custom-width-75 mb-5">
                                    <button className="background-color--grey--opacity100 text-black-custom font-family--helvetica border-none border-radius-10px custom-register--btn" type="button" onClick={() => this._register()}>Daftar</button>
                                </div>
                                
                            </form>
                        </div>
                        <div className="col-md-6 register__image--container">
                            <img className='register__image' src={MountainImage}/>
                        </div>
                    </div>
                </div>
        )

    }
}


export default RegisterForm
