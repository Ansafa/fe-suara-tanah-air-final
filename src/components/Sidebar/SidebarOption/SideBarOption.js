import React, { useState } from 'react';
import './SidebarOption.scss';
import { Modal, Button, Toast } from 'react-bootstrap';

const SidebarOption = ({
	option = '',
	Icon,
	logout,
	setIndexColorBorder,
	colorBorderIndex,
	idx,
	playlistId,
	redirectToPlaylistPage,
  login,
  register,
  isClicked
}) => {

  const [ isVisible, setIsVisible ] = useState(false);
  const [isShow, setIsShow] = useState(false)

	const playlistWithBorder = (idx) => {
		setIndexColorBorder(idx);
		if(logout){
			setIsVisible(true)
		}
	}

  const logoutHandler = () => {
    if(logout){
      setIsVisible(true)
    }
  }

  const redirectSidebar = () => {
    if(logout){
      setIsVisible(true)
    }
    else if(login){
      login()
    }else if(register){
      register()
    }
  }

  const handlelogout = () => {
    logout();
    setIsShow(true)
  }

	return (
    <>
      { (playlistId) ?
            <div
              onClick={() => playlistWithBorder(idx)}
              className={`sidebarOption ${colorBorderIndex === idx && idx != undefined ? 'border-left-blue' : null}`}
            >
              {Icon && <Icon className="sidebarOption__icon" />}
              {Icon ? <h4 >{option}</h4> : <p>{option}</p>}
            </div>
          :
            <>
              { isClicked ? 
                    <div
                      className='sidebarOption'
                      onClick={redirectSidebar}
                    >
                      {Icon && <Icon className="sidebarOption__icon color-white" />}
                      {Icon ? <h4 className=" color-white">{option}</h4> : <p>{option}</p>}
                    </div>
                  :
                    <div
                      className='sidebarOption'
                      onClick={redirectSidebar}
                    >
                      {Icon && <Icon className="sidebarOption__icon" />}
                      {Icon ? <h4 className="">{option}</h4> : <p>{option}</p>}
                    </div>
              }
            </>
      }
      <Modal show={isVisible} onHide={() => setIsVisible(false)}>
				<Modal.Header closeButton>
					<Modal.Title>Sign out</Modal.Title>
				</Modal.Header>
				<Modal.Body>Anda yakin ingin keluar?</Modal.Body>
				<Modal.Footer>
					<Button variant="secondary" onClick={() => setIsVisible(false)}>
						Tutup
					</Button>
					<Button variant="danger" onClick={handlelogout}>
						Keluar
					</Button>
				</Modal.Footer>
			</Modal>
      <Toast onClose={() => setIsShow(false)} show={isShow} delay={3000} autohide>
						<Toast.Body>Anda berhasil keluar dari akun anda</Toast.Body>
			</Toast>
    </>
	);
};

export default SidebarOption;
