import React, {useEffect, useState} from "react";
import {BrowserRouter as Router, Route, Link, useHistory} from 'react-router-dom';
import "./Sidebar.scss";
import withImportantStyle from 'react-with-important-style';
import HomeIcon from "@material-ui/icons/Home";
import LibraryMusicIcon from '@material-ui/icons/LibraryMusic';
import AccountBoxIcon from '@material-ui/icons/AccountBox';
import SidebarOption from "./SidebarOption/SideBarOption";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import {createBrowserHistory} from "history";
import Axios from 'axios';
import {Button} from "antd";
import burgerMenu from '../../public_images/burgerMenu.svg'
import IconSideBar from '../../public_images/logo.jpg'
import MenuIcon from '@material-ui/icons/Menu';
import { BASE_URL } from '../Constant'

const Sidebar = ({userId, user, logout, login, register, currUrl }) => {

  const [playlistData, setPlaylistData] = useState([])
  const [isFetched, setIsFetched] = useState(false)
  const [colorBorderIndex, setColorBorderIndex] = useState(-1);
  const history = createBrowserHistory({forceRefresh: true})
  const {isAdmin} = user
  const [marginLeft, setMarginLeft] = useState('-1000px');

  const params = window.location.pathname

  useEffect(() => {
    if(user){
      getPlaylist(user.playlists)
    }
  }, [user])

  const setIndexColorBorder = (id) => {
    setColorBorderIndex(id);
  }

  const getPlaylist = (playlists) => {
    playlists &&
    playlists.map(playlistId => (
      Axios.get(`${BASE_URL}/playlist/${playlistId}`)
        .then(res => {
          const data = res.data
          setPlaylistData([...playlistData, playlistData.push(data)])
          setPlaylistData([...playlistData])
        })
    ))
    setIsFetched(true)
  }

  const redirectToPlaylistPage = (playlistId) => {
    history.push({
      pathname: `${BASE_URL}/profile/${userId}/playlist/${playlistId}`,
      state: {currentUser: user}
    })
  }

  const handleShow = () => {
    setMarginLeft('0px');
  }

  const handleHide = () => {
    setMarginLeft('-1000px');
  }


  return (
    <div>
      <div className='button-test-container'>
        <MenuIcon className='button-test' onClick={() => handleShow()}/>
      </div>
      <div className='sidebar'>
        <div className='sidebar-no-collapse'>
          <div className='top-section'>
            <p className="mt-4 ml-1 sidebar-no-collapse__logo font-logo">Suara Tanah Air</p>
            { currUrl === 'home' ? 
              <Link
                exact to='/'
                style={{textDecoration: 'none' }}>
                <SidebarOption Icon={HomeIcon} isClicked={true} currUrl={currUrl} option="Beranda"/>
              </Link>
               :
               <Link
                exact to='/'
                style={{textDecoration: 'none' }}>
                <SidebarOption Icon={HomeIcon} currUrl={currUrl} option="Beranda"/>
              </Link>
            }

            {userId &&
                <>{
                  currUrl === 'profile' ?
                  <Link to={`/profile/${userId}`}
                        style={{textDecoration: 'none'}}>
                    <SidebarOption Icon={AccountBoxIcon}
                                  option="Profil Anda"
                                  isClicked={true}/>
                  </Link> 
                  : 
                  <Link to={`/profile/${userId}`}
                      style={{textDecoration: 'none'}}>
                  <SidebarOption Icon={AccountBoxIcon} option="Profil Anda"/>
                </Link>
                  }
                </>
            }

             { 
              currUrl === 'songList' ? 
                <Link to="/song-list"
                      style={{textDecoration: 'none'}}>
                  <SidebarOption Icon={LibraryMusicIcon}  isClicked={true} option="Lagu-Lagu"/>
                </Link> 
                :
                <Link to="/song-list"
                      style={{textDecoration: 'none'}}>
                  <SidebarOption Icon={LibraryMusicIcon}
                                option="Lagu-Lagu"/>
                </Link>
            }
            {
              isAdmin &&
                <>
                  {
                    currUrl === 'admin' ? 
                      <Link to="/admin/manage-songs" style={{textDecoration: 'none'}}>
                        <SidebarOption Icon={AssignmentIndIcon}
                                      option="Admin"
                                      isClicked={true}/>
                      </Link> : 
                      <Link to="/admin/manage-songs" style={{textDecoration: 'none'}}>
                        <SidebarOption Icon={AssignmentIndIcon}
                                      option="Admin"/>
                      </Link>
                  }
                </>
            }

            <div style={{height: '20px'}}></div>
            <strong className="sidebar-no-collapse__title">PLAYLISTS</strong>
            <hr/>

            <div className='sidebar-no-collapse__option'>
              {
                isFetched &&
                playlistData.map((data, idx) => (
                  <Link
                    to={`/profile/${userId}/playlist/${data._id}`}
                    style={{textDecoration: 'none'}}>
                    <SidebarOption playlistId={data._id} option={data.title}
                                   setIndexColorBorder={setIndexColorBorder} idx={idx}
                                   colorBorderIndex={colorBorderIndex}
                    />
                  </Link>
                ))
              }
            </div>
          </div>

          <div className='bottom-section'>
            {
              userId ?
                <Link style={{textDecoration: 'none'}}>
                  <SidebarOption
                    logout={logout}
                    Icon={ExitToAppIcon}
                    option="Keluar"/>
                </Link>
                :
                <>
                  <Link exact to='/login' style={{textDecoration: 'none'}}><SidebarOption login={login} option="Masuk"/></Link>
                  <Link exact to='/register' style={{textDecoration: 'none'}}><SidebarOption register={register} option="Daftar"/></Link>
                </>
            }
          </div>
        </div>
      </div>

      <div className='sidebar-2' style={{marginLeft : marginLeft}}>
        <div className='sidebar-2-no-collapse'>
          <div className='top-2-section'>
            <img
              className="sidebar-2-no-collapse__logo"
              src="https://getheavy.com/wp-content/uploads/2019/12/spotify2019-830x350.jpg"
              alt=""
            />
            <MenuIcon className='button-close' onClick={() => handleHide()}/>
            <Link
              exact to='/'
              style={{textDecoration: 'none'}}>
              <SidebarOption Icon={HomeIcon} option="Beranda"/>
            </Link>

            {userId &&
            <Link to={`/profile/${userId}`}
                  style={{textDecoration: 'none'}}>
              <SidebarOption Icon={AccountBoxIcon}
                             option="Profil Anda"/>
            </Link>
            }

            <Link to="/song-list"
                  style={{textDecoration: 'none'}}>
              <SidebarOption Icon={LibraryMusicIcon}
                             option="Lagu-Lagu"/>
            </Link>

            {
              isAdmin &&
              <Link to="/admin/manage-songs" style={{textDecoration: 'none'}}>
                <SidebarOption Icon={AssignmentIndIcon}
                               option="Admin"/>
              </Link>
            }

            <div style={{height: '20px'}}></div>
            <strong className="sidebar-2-no-collapse__title">PLAYLISTS</strong>
            <hr/>

            <div className='sidebar-2-no-collapse__option'>
              {
                isFetched &&
                playlistData.map((data, idx) => (
                  <Link
                    to={`/profile/${userId}/playlist/${data._id}`}
                    style={{textDecoration: 'none'}}>
                    <SidebarOption playlistId={data._id} option={data.title}
                                   setIndexColorBorder={setIndexColorBorder} idx={idx}
                                   colorBorderIndex={colorBorderIndex}
                    />
                  </Link>
                ))
              }
            </div>
          </div>

          <div className='bottom-2-section'>
            {
              userId ?
                <Link style={{textDecoration: 'none'}}>
                  <SidebarOption
                    logout={logout}
                    Icon={ExitToAppIcon}
                    option="Sign Out"/>
                </Link>
                :
                <>
                  <Link exact to='/login' style={{textDecoration: 'none'}}><SidebarOption login={login} option="Sign In"/></Link>
                  <Link exact to='/register' style={{textDecoration: 'none'}}><SidebarOption register={register} option="Register"/></Link>
                </>
            }
          </div>
        </div>
      </div>
    </div>
  );
}

export default Sidebar;