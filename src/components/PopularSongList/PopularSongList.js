import React, { useState } from "react";
import PopularSong from "./PopularSong/PopularSong";
import SongPlayer from "../AudioPlayer/AudioPlayer";
import { Modal, Button } from "react-bootstrap";
import "./PopularSongList.scss";
import Axios from "axios";
import Toast from 'react-bootstrap/Toast';
import { useHistory } from "react-router-dom";
import { BASE_URL } from '../Constant'

const PopularSongList = ({
  popularSongs,
  setColorRowIndex,
  colorRowIndex,
  userId,
  handleSetGLobalCurrSong,
  setIsReportShow,
  getPopularSong
}) => {
  const [currSong, setCurrSong] = useState({});
  const [currentUser, setCurrentUser] = useState({});
  const [songToBeDeleted, setSongToBeDeleted] = useState({});
  const [songToBeReported, setSongToBeReported] = useState({});
  const [isReportVisible, setIsReportVisible] = useState(false);
  const [isShow, setIsShow] = useState(false)

   const history = useHistory();


  const setReportedSong = (val, data) => {
    setIsReportVisible(val)
    setSongToBeReported(data)
  }

  const reportUploadSong = (data) => {
    const payload = {
      userId
    }
    Axios.post(`${BASE_URL}/uploadSong/${data._id}/report`, payload)
    .then(res => {
      setSongToBeReported({})
    })
    setIsReportShow(true)
    setIsReportVisible(false)
  }

  
  const goDetail = (song) => {
        localStorage.setItem('currSong', JSON.stringify(song))
        history.push(`/songpage/${song._id}`)
  }


  return (
    <div className="w-100">
      <div className="popular-song-wrapper">
        <center>
          <h4 style={{ color: "white", padding: "5px" }}>Lagu Popular</h4>
        </center>
        <div className="row pt-3">
          <div className="col pl-5" >
            <h4 style={{ color: "white" }} className="custom-font-size-12">
              Judul Lagu
            </h4>
          </div>
          <div className="col show">
            <h4 style={{ color: "white" }} className="custom-font-size-12">
              Asal Daerah
            </h4>
          </div>
          <div className="col new-year show">
            <h4 style={{ color: "white" }} className="custom-font-size-12">
              Uploader / Pencipta
            </h4>
          </div>
          <div className='col show'>
          </div>
        </div>
        {popularSongs.map((song, i) => {
          return (
            <PopularSong
              data={song}
              userId={userId ? userId : null}
              setCurrSong={setCurrSong}
              index={i}
              colorRowIndex={colorRowIndex}
              setColorRowIndex={setColorRowIndex}
              setReportedSong={setReportedSong}
              handleSetGLobalCurrSong={handleSetGLobalCurrSong}
              getPopularSong={getPopularSong}
            />
          );
        })}
      </div>
      <Modal show={isReportVisible} onHide={() => setIsReportVisible(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Laporkan Lagu</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {
            `Apakah Anda yakin lagu ${songToBeReported.songName} tidak sesuai judul / mengandung SARA`
          }
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setIsReportVisible(false)}>
            Tutup
          </Button>
          <Button
            variant="danger"
            onClick={() => reportUploadSong(songToBeReported)}
          >
            Iya, Laporkan Lagu
          </Button>
        </Modal.Footer>
      </Modal>
      <div style={{height: '50px'}}> </div>
    </div>
  );
};

export default PopularSongList;
