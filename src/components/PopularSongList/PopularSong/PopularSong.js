import React from 'react'
import Axios from 'axios';
import {FaPen, FaTrash} from 'react-icons/fa';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import FavoriteIcon from '@material-ui/icons/Favorite';
import { BASE_URL } from '../../Constant'

const PopularSong = ({
                       data, setCurrSong, index, setColorRowIndex, colorRowIndex,
                       userId, setReportedSong,
                       handleSetGLobalCurrSong,
                       isUserProfile,
                       getPopularSong, 
                     }) => {

  const {_id: songId, likes, songName, songCover, uploader, title, writer, origin} = data

  const newData = {
    ...data,
    index
  }

  const playSong = (newData, index) => {
    setColorRowIndex(index);
    setCurrSong(newData);
    handleSetGLobalCurrSong(newData, 'popular')
  }

  const validateLike = () => {
    if (typeof data === 'object') {
      for (let i = 0; i < likes.length; i++) {
        if (likes[i] === userId) {
          return true;
        }
      }
      return false;
    }
  }

  const handleLike = async () => {
    const payload = {
      userId,
      songData: data
    }
    if (!validateLike()) {
      if (data.uploader) {
        Axios.post(`${BASE_URL}/uploadSong/${songId}/likes/${userId}`, payload)
          .then(res => {
            getPopularSong()
          })
      } else {
        Axios.post(`${BASE_URL}/list/${songId}/likes/${userId}`, payload)
          .then(res => {
            getPopularSong()
          })
      }
    } else {
      if (data.uploader) {
        Axios.delete(`${BASE_URL}/uploadSong/${songId}/likes/${userId}`)
          .then(res => {
            getPopularSong()
          })
      } else {
        Axios.delete(`${BASE_URL}/list/${songId}/likes/${userId}`)
          .then(res => {
            getPopularSong()
          })
      }
    }
  }

  return (
    <div>
      <hr className="customize-hr mt-1 mb-1 height-1px"/>
      <div
        className={`row songs ml-0 mr-0 ${colorRowIndex === index && index != undefined ? 'row-light-grey' : null}`}>
        <div onClick={() => playSong(newData, index)} className="col">
          <h6 className="title-songs  ml-02">
            {title ? title : songName}
          </h6>
        </div>
        <div className="col show">
          <h6 style={{color: 'white'}}
              className="custom-heading-songlist font-family--helvetica">{origin ? origin : songCover.origin}</h6>
        </div>
        <div className="col show">
          <h6 style={{color: 'white'}} className="font-family--helvetica ml-4">{writer ? writer : uploader.username}</h6>
        </div>
        <div className="edit-icon col">
          <div className='row'>
            <div className='col'>
              {userId && <div style={{color: 'white'}} onClick={handleLike}>{!validateLike() ? <FavoriteBorderIcon/> :
                <FavoriteIcon/>}</div>}
            </div>
            <div className='col'>
              {
                data.reports &&
                <p style={{color: 'white'}} onClick={() => setReportedSong(true, data)}> Report </p>
              }
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default PopularSong
