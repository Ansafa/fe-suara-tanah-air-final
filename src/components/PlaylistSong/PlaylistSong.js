import React, { useEffect, useState } from 'react';
import { useLocation } from "react-router-dom";
import Mountain from '../../public_images/defaultPlaylist.svg';
import { FaPen, FaTrash } from 'react-icons/fa';
import './PlaylistSong.scss'
import Axios from 'axios';
import Song from '../Song/Song';
import SongPlayer from '../AudioPlayer/AudioPlayer';
import {getCredential, removeCredential, setCredential} from '../Utils';
import {Redirect} from 'react-router-dom';
import UploadImage from '../UploadImage/UploadImage'
import { Modal, Button } from 'react-bootstrap';
import Toast from 'react-bootstrap/Toast';
import { BASE_URL } from '../Constant'
import Spinner from 'react-bootstrap/Spinner';

const PlaylistSong = ({
        match, handleGetCurrSongGlobal,
        handleGetSongListGlobal, setColorRowIndex,
        colorRowIndex}) => {
  const location = useLocation();
  const [currentUser, setCurrentUser] = useState({})
  const [playlistSongs, setPlaylistSongs] = useState([])
  const [currSong, setCurrSong] = useState({})
  const [playlist, setPlaylist] = useState({})
  const [isEdit, setIsEdit] = useState(null)
  const [newPlaylistTitle, setNewPlaylistTitle] = useState({})
  const [isRedirect, setIsRedirect] = useState(false)
  const [url, setUrl] = useState('')
  const [isVisible, setIsVisible ] = useState(false);
  const [ show, setShow ] = useState(false);
  const [isRemoveShow, setIsRemoveShow] = useState(false);
  const [isEditImg, setIsEditImg] = useState(false)
  const [isImageToast, setIsImageToast] = useState(false)
  const [isError, setIsError] = useState(false)

    const { _id:playlistId, title, creator, imgPath } = playlist
    const { id:userId, username } = currentUser

    const locState = location.state

    useEffect(() => {
        getLoggedInUser()
        getPlaylist(match.params.playlistId)
    }, [match.params.playlistId])

    const getPlaylist = (playlistId) => {
        Axios.get(`${BASE_URL}/playlist/${playlistId}`)
        .then(res => {
            setPlaylist(res.data)
            getPlaylistSong(playlistId)
        })
  
    }


    const getLoggedInUser = async () => {
        let user = JSON.parse(getCredential());
        if (user) {
            Axios.get(`${BASE_URL}/userProfile/${user.id}`)
            .then(res => {
                setCurrentUser(res.data)
            })
        }
    }

    const handleOnChange = (e) => {
        setNewPlaylistTitle({ ...newPlaylistTitle, [e.target.name]: e.target.value })
    }

    const toggleEdit = () => {
        if (isEdit) {
            setIsEdit(false)
        } else {
            setNewPlaylistTitle(playlist)
            setIsEdit(true)
        }
    }

    const updatePlaylist = async () => {
        if(!title.trim() || !newPlaylistTitle.title.trim()){
            return setIsError(true)
        }
        Axios.put(`${BASE_URL}/playlist/${playlistId}`, newPlaylistTitle)
        .then(res => {
            setIsEdit(false);
            setIsImageToast(true)
            setPlaylist(newPlaylistTitle)
        })
         
    }

    const getPlaylistSong = (playlistId) => {
        Axios.get(`${BASE_URL}/playlist/${playlistId}/songs`)
        .then(res => {
            const data = res.data[0]
            setPlaylistSongs(data.playlistSongs)
        })
    }

    const redirectAfterDelete = () => {
         const newUrl = `/profile/${currentUser.id}`;
         window.location.href = newUrl;
    }

    const deletePlaylist = () => {
       Axios.delete(`${BASE_URL}/userProfile/${userId}/playlist/${playlistId}`)
       .then(res => {
            setIsRedirect(true)
            redirectAfterDelete()
       })
    }

    const handleUploadImage = () => {
       return <UploadImage currPlaylistId={playlistId} click={true} />
    }

    const handleSetGLobalCurrSong = (songData) => {
        localStorage.removeItem('songList');
        localStorage.setItem('currSong', JSON.stringify(songData))
        if (playlistSongs) {
            localStorage.setItem('songList', JSON.stringify(playlistSongs))
        }
        handleGetCurrSongGlobal()
        handleGetSongListGlobal()
    }


    return (
        isRedirect ? <Redirect url={url} /> :
        <div className="w-100 playlistSong-page pt-5">
           <div className="row">
               <div className="col-sm-1">
               </div>
               <div className="col-sm-4">
                   <h4 className="font-family--helvetica position-text-playlist">
                    {!isEdit ? <h4 className='title'>{title}</h4> :
                    <div>
                    <h6>Nama Playlist</h6>
                    <input style={{fontSize: '15px',  width:'50%', height: '2em'}} className='search-input'
                   name='title' type='text' value={newPlaylistTitle.title} onChange={handleOnChange}/>
                   </div>}
                   </h4>
               </div>
               {
                        isEdit ?
                                <div className='col-sm-3 position-button-edit pl-0 pr-0'>
                                            <button
                                            type="button"
                                            class="btn btn-danger mr-3 mb-2"
                                            onClick={() => setIsEdit(false)}
                                            >
                                            Cancel
                                            </button>
                                            <button
                                            type="button"
                                            class="btn btn-primary mr-3 mb-2"
                                            onClick={updatePlaylist}
                                            >
                                            Submit
                                            </button>
                            </div> : null
                    }
                  {
                    !isEdit &&
                        <div className="col-sm-3 position-pencil">
                        <>
                            <FaPen className='edit-icon' onClick={toggleEdit} style={{}}/>
                            <span className="ml-4 edit-icon" >
                                <FaTrash onClick={() => setIsVisible(true)} />
                            </span>
                        </>
                    </div>
                  }
               <div className="col-sm-4 text-center image-playlist">
                   <img className="image-playlist" src={imgPath} alt="..." />
                   <div className='image-playlist-layer'>
                        <div className="image-playlist-description"><FaPen onClick={() => setIsEditImg(true)} className='edit-pen-icon' /> </div>
                   </div>
               </div>
           </div>
           <div className="row">
               {isEdit?  <hr className="customize-hr-playlist-song position-hr" style={{marginTop: '1em'}}/> :  <hr style={{marginTop: '-1em'}} className="customize-hr-playlist-song position-hr"/>}
           </div>

          <div className='row' style={{margin: '1em 0'}}>
            {isEditImg &&<><div className='col'></div>
            <div className='col' style={{textAlign: 'center'}}><UploadImage 
                                                                    setIsEditImg={setIsEditImg} 
                                                                    userId={userId} getPlaylist={getPlaylist} 
                                                                    playlistId={playlistId} 
                                                                    setIsImageToast={setIsImageToast}
                                                                    />
            </div> </>}
          </div>

            {creator ? <div className="songPlaylist-wrapper container break-container mt-3">
                <div className="row custom-padding-5 pt-3">
                    <div className="col" style={{ paddingLeft: "2.5em" }}>
                        <h4 style={{ color: 'white' }} className="custom-font-size-12">Judul Lagu</h4>
                    </div>
                    <div className="col show" style={{ marginLeft:'-1em'}}>
                        <h4 style={{ color: 'white' }} className="custom-font-size-12 show">Asal Daerah</h4>
                    </div>
                    <div className="col show">
                        <h4 style={{ color: 'white' }} className="custom-font-size-12 show">Pencipta</h4>
                    </div>
                    <div className='col show'>

                    </div>
                </div>
                {

                    playlistSongs.map((song, i) => {
                        return <Song
                            data={song}
                            isPlaylist={true}
                            currSong={currSong}
                            setCurrSong={setCurrSong}
                            currentUser={currentUser}
                            getPlaylistSong={getPlaylistSong}
                            playlistId={playlistId}
                            index={i}
                            colorRowIndex={colorRowIndex}
                            setColorRowIndex={setColorRowIndex}
                            setShow={setShow}
                            setIsRemoveShow={setIsRemoveShow}
                            handleSetGLobalCurrSong={handleSetGLobalCurrSong}
                        />
                    })
                }
            </div> :<center className='mt-5'> <Spinner variant='dark'  animation="border" /> </center>}
            <Modal show={isVisible} onHide={() => setIsVisible(false)}>
				<Modal.Header closeButton>
					<Modal.Title>Hapus Playlist</Modal.Title>
				</Modal.Header>
				<Modal.Body>Anda yakin ingin menghapus playlist ini?</Modal.Body>
				<Modal.Footer>
					<Button variant="secondary" onClick={() => setIsVisible(false)}>
						Tutup
					</Button>
					<Button variant="danger" onClick={() => deletePlaylist()}>
						Hapus Playlist
					</Button>
				</Modal.Footer>
			</Modal>
            <div className="toast-notification position-toast">
					<Toast onClose={() => setShow(false)} show={show} delay={3000} autohide>
						<Toast.Body>Anda berhasil unliked lagu!</Toast.Body>
					</Toast>
			</div>
            <div className="toast-notification position-toast">
					<Toast onClose={() => setIsRemoveShow(false)} show={isRemoveShow} delay={3000} autohide>
						<Toast.Body>Anda berhasil remove lagu dari playlist</Toast.Body>
					</Toast>
			</div>
            <div className="toast-notification position-toast">
					<Toast onClose={() => setIsImageToast(false)} show={isImageToast} delay={3000} autohide>
						<Toast.Body>Anda berhasil update playlist</Toast.Body>
					</Toast>
			</div>
             <div className="toast-notification position-toast-error">
					<Toast onClose={() => setIsError(false)} show={isError} delay={3000} autohide>
						<Toast.Body>Nama playlist tidak boleh kosong!</Toast.Body>
					</Toast>
			</div>
        </div>
    )
}

export default PlaylistSong