import React, {useEffect, useState} from "react";
import {FaPen, FaSearch} from "react-icons/fa";
import {useHistory} from "react-router-dom";
import Axios from "axios";
import {BASE_URL} from "../Constant";
import Playlist from "../Playlist/Playlist";
import LikedSong from "../LikedSong/LikedSong";
import {getCredential, removeCredential, setCredential} from "../Utils";
import "./UserProfile.scss";
import {Carousel} from "react-bootstrap";
import UploadedSongList from "../UploadedSongList/UploadedSongList";
import SongPlayer from "../AudioPlayer/AudioPlayer";
import EmptyPic from "../../public_images/empty.svg";
import Toast from 'react-bootstrap/Toast';
import {createBrowserHistory} from "history";
import SearchSong from '../SearchSong/SearchSong';
import Spinner from 'react-bootstrap/Spinner';

const UserProfile = ({
    match, handleGetCurrSongGlobal, handleGetSongListGlobal, 
    setColorRowIndex, colorRowIndex, setCurrUrl
  }) => {
  const [currentUser, setCurrentUser] = useState({});
  const userId = match.params.userId;
  const [isEdit, setIsEdit] = useState(false);
  const [playlists, setPlaylists] = useState([]);
  const [likedSongs, setLikedSongs] = useState([]);
  const [isCreatePlaylist, setIsCreatePlaylist] = useState(false);
  const [createdPlaylist, setCreatedPlaylist] = useState("");
  const [uploadedSongs, setUploadedSongs] = useState([{songName: ''}])
  const [index, setIndex] = useState(0);
  const [ isShow, setIsShow ] = useState(false);
  const [isShowPlaylist, setIsShowPlaylist] = useState(false)
  const [isShowDeleteUploadSong, setIsShowDeleteUploadSong] = useState(false)
  const [newUserEdit, setNewUserEdit] = useState({})
  const [isError, setIsError] = useState(false)

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };

  const { username, email } = currentUser;
  const history = useHistory();

  const getLoggedInUser = async () => {
    let user = JSON.parse(getCredential());
    if (user) {
      Axios.get(`${BASE_URL}/userProfile/${user.id}`).then((res) => {
        const userData = res.data;
        setCurrentUser(userData);
        getUserUploadedSong(userData.id);
        setLikedSongs(userData.likedMusic);
        getSongsFromUserPlaylist(userData.playlists);
      });
    }
  };

  useEffect(() => {
    getLoggedInUser();
    setCurrUrl('profile')
  }, []);

  const updateUser = async () => {
    if (!username.trim() || !newUserEdit.username.trim()) {
      return setIsError(true)
    }
    Axios.put(`${BASE_URL}/userProfile/${userId}/update`, newUserEdit).then((res) => {
      setIsShow(true)
      setIsEdit(false);
      setCurrentUser(newUserEdit)
      setCredential(res.data);
    });
  };

  const getSongsFromUserPlaylist = (value) => {
    setPlaylists(value);
  };

  const toggleEdit = () => {
    if (isEdit) {
      setIsEdit(false);
    } else {
      setNewUserEdit(currentUser)
      setIsEdit(true);
    }
  };

  const handleOnChange = (e) => {
    setNewUserEdit({ ...newUserEdit, [e.target.name]: e.target.value });
  };

  const redirectToLikedSongPage = () => {
    history.push({
      pathname: `/profile/${userId}/likedSongs`,
      state: { currentUser, userId: currentUser.id },
    });
  };

  const toggleCreatePlaylist = () => {
    if (!isCreatePlaylist) {
      setIsCreatePlaylist(true);
    } else {
      setIsCreatePlaylist(false);
    }
  };

  const handleCreatePlaylist = (e) => {
    setCreatedPlaylist(e.target.value);
  };

   const redirectAfterCreate = () => {
         const newUrl = `/profile/${currentUser.id}`;
         window.location.href = newUrl;
    }

  const createPlaylist = () => {
    if (!createdPlaylist.trim()) {
      return setIsError(true)
    }
    const payload = {
      title: createdPlaylist,
      userId
    };
    Axios.post(`${BASE_URL}/playlist`, payload).then((res) => {
      setIsShowPlaylist(true)
      setCreatedPlaylist("");
      setIsCreatePlaylist(false);
      getLoggedInUser();
      redirectAfterCreate();
    });
  };

  const getUserUploadedSong = (userId) => {
    Axios.get(`${BASE_URL}/userProfile/${userId}/uploadSong`).then((res) => {
      const data = res.data[0];
      setUploadedSongs(data.uploadMusic);
    });
  };

  const handleCancelPlaylist = () => {
    setIsCreatePlaylist(false)
    setCreatedPlaylist('')
  }

  const handleSetGLobalCurrSong = (songData) => {
        localStorage.removeItem('songList');
        localStorage.setItem('currSong', JSON.stringify(songData))
        localStorage.setItem('songList', JSON.stringify(uploadedSongs))
        handleGetCurrSongGlobal()
        handleGetSongListGlobal()
    }

  console.log(uploadedSongs);

  return (
    <div className="w-100 tes ">
      <div className="content-up background-image-profile height-50vh">
        <div className="row justify-content-between">
          <div className="text-page-user">
            <h4 className="mt-3 font-family--helvetica text-black-custom profile-header">
              Profil Anda
            </h4>
          </div>
          <div className="search-song">
            <div>
              <SearchSong />
            </div>
          </div>
        </div>
        <div className="all-content-info position-relative-content mt-4">
          <div className="container">
            <div className="row justify-content-between">
              <div className="user-fullname ml-5 pt-2 font-family--helvetica">
                {!isEdit ? (
                  <h4 className="username font-family--helvetica user-fullname">
                    {username}
                  </h4>
                ) : (
                  <div>
                      <p>Username</p>
                      <input
                        style={{ fontSize: "15px" }}
                        name="username"
                        type="text"
                        value={newUserEdit.username}
                        onChange={handleOnChange}
                        className='search-input'
                        placeholder='Masukkan username'
                      />
                  </div>
                )}
              </div>
              {!isEdit && (
                <div style={{cursor: "pointer"}} className="user-pencil mr-3">
                  <FaPen onClick={toggleEdit}/>
                </div>
              )}
            </div>
          </div>
          <div className="line-horizontal">
            <hr className="change-margin-left-hr customize-hr w-50 mt-1 mb-1 height-1px" />
          </div>
          <div className="ml-5 email-address-info">{email}</div>
          <div className="row justify-content-around">
            <div className="col-sm-6"></div>
            {isEdit ? (
              <div className="col-sm-6 text-right">
                <button
                  type="button"
                  class="btn btn-danger mr-3 mb-2"
                  onClick={() => setIsEdit(false)}
                >
                  Cancel
                </button>
                <button
                  type="button"
                  class="btn btn-primary mr-3 mb-2"
                  onClick={updateUser}
                >
                  Submit
                </button>
              </div>
            ) : null}
          </div>
        </div>
      </div>
      <div className="container py-3">
        {!isCreatePlaylist ? (
          <div className='row'>
            <div className="col">
              <div className="button-container">
                <button
                  type="button"
                  onClick={toggleCreatePlaylist}
                  class="btn btn-primary mb-2 mt-2"
                >
                  Tambah Playlist
                </button>
              </div>
            </div>
          </div>
          ) : (
            <div className="col-sm-6 row pt-3">
            <div className="row padding-15px">
								<h5>Tambah Playlist</h5>
							</div>
            <div className='row'>
                <div className="col-sm-5 mt-1 mr-2">
                  <input placeholder='Masukkan nama playlist' className='search-input'
                    style={{width: '120%'}} value={createdPlaylist} onChange={handleCreatePlaylist} />
                </div>
                <div className="col-sm-2">
                  <button type="button" class="btn btn-danger mr-3 mb-5" onClick={handleCancelPlaylist}>
                    Cancel
                  </button>
                </div>
                <div className="col-sm-2">
                  <button type="button" class="btn btn-primary mr-3" onClick={createPlaylist}>
                    Submit
                  </button>
                </div>
              </div>
						</div>
          )}
        </div>
      <div className="carousel-gallery pt-2">
        { username ? <Carousel  activeIndex={index} onSelect={handleSelect}>
          {likedSongs.length > 0 && playlists.length === 0 ? (

            <Carousel.Item>
              <LikedSong redirectLikedSong={redirectToLikedSongPage}/>{' '}
            </Carousel.Item>
          ) : null}

          {playlists.length !== 0 &&
          playlists.map((playlist, index) => {
            return (
              <Carousel.Item style={{marginTop: '20px'}}>
                <div className="container">
                  <div className="row">
                    {likedSongs.length > 0 && index === 0 ? (
                      <LikedSong redirectLikedSong={redirectToLikedSongPage}/>
                    ) : null}
                    <Playlist
                      playlistId={playlists[index]}
                      userId={userId}
                      currentUser={currentUser}
                    />
                    <Playlist
                      playlistId={playlists[index + 1]}
                      userId={userId}
                      currentUser={currentUser}
                    />
                    {index !== 0 ? (
                      <Playlist
                        playlistId={playlists[index + 2]}
                        userId={userId}
                        currentUser={currentUser}
                      />
                    ) : null}
                  </div>
                </div>
              </Carousel.Item>
            );
          })}
        </Carousel> : <center> <Spinner variant='dark'  animation="border" /> </center>}
      </div>
      {uploadedSongs[0].songName ? (
        <UploadedSongList
          userId={userId}
          uploadedSongs={uploadedSongs}
          getUserUploadedSong={getUserUploadedSong}
          setIsShowDeleteUploadSong={setIsShowDeleteUploadSong}
          handleSetGLobalCurrSongUserProfile={handleSetGLobalCurrSong}
          setColorRowIndex={setColorRowIndex}
          colorRowIndex={colorRowIndex}
          isUserProfile={true}
        />
      ) : null}

      <div className="toast-notification position-toast">
					<Toast onClose={() => setIsShow(false)} show={isShow} delay={3000} autohide>
						<Toast.Body>Anda berhasil memperbarui profil Anda!</Toast.Body>
					</Toast>
			</div>
      <div className="toast-notification position-toast">
					<Toast onClose={() => setIsShowPlaylist(false)} show={isShowPlaylist} delay={3000} autohide>
						<Toast.Body>Anda berhasil membuat playlist!</Toast.Body>
					</Toast>
			</div>
      <div className="toast-notification position-toast">
					<Toast onClose={() => setIsShowDeleteUploadSong(false)} show={isShowDeleteUploadSong} delay={3000} autohide>
						<Toast.Body>Anda berhasil menghapus lagu Anda!</Toast.Body>
					</Toast>
			</div>
       <div className="toast-notification position-toast-error">
					<Toast onClose={() => setIsError(false)} show={isError} delay={3000} autohide>
						<Toast.Body>Nama tidak boleh kosong!</Toast.Body>
					</Toast>
			</div>
      <div style={{height: '150px'}}> </div>
    </div>
  );
};

export default UserProfile;