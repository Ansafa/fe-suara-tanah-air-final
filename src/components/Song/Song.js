import React, {useState} from 'react'
import Axios from 'axios';
import {Table} from 'antd'
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import FavoriteIcon from '@material-ui/icons/Favorite';
import 'react-h5-audio-player/lib/styles.css';
import DeleteIcon from '@material-ui/icons/Delete';
import './Song.scss';
import VolumeUpIcon from '@material-ui/icons/VolumeUp';
import DetailsIcon from '@material-ui/icons/Details';
import { BASE_URL } from '../Constant'

const Song = ({data, goDetail, setCurrSong, currentUser, isLikedPage, 
                getSong, index, isPlaylist, getLikedSong, playlistId,
                colorRowIndex, setColorRowIndex, getPlaylistSong, isSongList,
                setShow, setIsRemoveShow, handleSetGLobalCurrSong
    }) => {

    const { likes, _id:songId, pathInstrumental:instrumental } = data;

    const { id:userId, likedMusic } = currentUser;

    const newData = { 
        ...data,
        index
     }

    const instrumentalData = {
        ...data,
        index,
        isPlayInstrumental: true
    }
    const validateLike = () => {
        if(typeof data === 'object'){
            for(let i=0; i < likes.length; i++){
                if(likes[i] === userId){
                    return true;
                }
            }
            return false;
        }
    }

    const handleLike = async () => {
        const payload = { 
            userId,
            songData: data
        }
        const variable = {}
        if(!validateLike()){
            if(data.uploader){
                console.log(data);
            }else{
                Axios.post(`${BASE_URL}/list/${songId}/likes/${userId}`, payload)
                .then(res => {
                    if(!isLikedPage && !isPlaylist){
                        getSong(variable)
                    }else{
                        getPlaylistSong(playlistId)
                    }
                })
            }
        }else{
            setShow(true);
            if(data.uploader){
                Axios.delete(`${BASE_URL}/uploadSong/${songId}/likes/${userId}`)
                .then(res => {
                    getLikedSong(userId)
                })
            }
            else{
                Axios.delete(`${BASE_URL}/list/${songId}/likes/${userId}`)
                .then(res => {
                    if(!isLikedPage && !isPlaylist){
                        getSong(variable)
                    }else{
                        if(isPlaylist){
                            getPlaylistSong(playlistId)
                        }else{
                            getLikedSong(userId)
                        }
                    }   
                })
            }
        }
    }
    

    const removeFromPlaylist = (data) => {
        Axios.delete(`${BASE_URL}/playlist/${playlistId}/song/${data._id}`)
        .then(res => {
            console.log(res)
            getPlaylistSong(playlistId)
        })
        setIsRemoveShow(true)
    }

    const handleSetSong = (data) => {
        console.log('data',data);
        handleSetGLobalCurrSong(data)
        setCurrSong(data)
    }

    return (
        <div>
            <hr  className="customize-hr mt-1 mb-1 height-1px" />
            <div onClick={() => setColorRowIndex(index)} className={`row songs ml-0 mr-0  ${colorRowIndex === index && index != undefined ? 'row-light-grey' : null}`} style={{cursor: 'pointer'}} >
                <div className="col">
                    <h6 onClick={() => handleSetSong(newData)} style={{cursor: 'pointer'}} className="title-songs  ml-02">
                        {data.title ? data.title : data.songName}
                    </h6>
                </div>
               <div className={isSongList ? 'col show' : 'col show'}>
                    <h6 style={{color: 'white'}} className="font-family--helvetica">{data.origin ? data.origin : data.songCover.origin}</h6>
                </div>
                <div className="col show">
                    <h6 style={{color: 'white'}} className="font-family--helvetica">{data.writer}</h6>
                </div>
                <div className="col" style={{textAlign: 'center'}}>
                  {
                    isPlaylist &&
                    <DeleteIcon className='grey-hover mr-5' style={{color: 'white'}} onClick={() => removeFromPlaylist(data)} />
                  }
                  {
                      userId &&
                    <span style={{textAlign: 'center'}} style={{color: 'white'}} className="" onClick={handleLike}>{!validateLike() ? <FavoriteBorderIcon/> : <FavoriteIcon/> }</span>
                  }
                </div>
                { isSongList &&
                <div className='col' style={{textAlign: 'center'}}>
                    <p onClick={() => goDetail(data)} className='go-detail'>Detail</p>
                </div>
                }
            </div>
        </div>

  )
}

export default Song
