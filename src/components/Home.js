import React, { useState, useEffect } from 'react'
import { getCredential, removeCredential } from './Utils';
import Axios from 'axios';
import './Home.css'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import Sidebar from './Sidebar/Sidebar';
import UserProfile from './UserProfile/UserProfile';
import SongList from './SongList/SongList';
import SongPage from './SongPage/SongPage';
import LoginForm from './LoginForm/LoginForm';
import RegisterForm from './RegisterForm/RegisterForm';
import HomePage from './HomePage/HomePage';
import LikedSongList from './LikedSong/LikedSongList/LikedSongList';
import PlaylistSong from './PlaylistSong/PlaylistSong';
import AdminPage from './AdminPage/AdminPage';
import SongPlayer from './AudioPlayer/AudioPlayer';
import { BASE_URL } from './Constant'

const Home = () => {

    const [currentUser, setCurrentUser] = useState({})
    const [url, setUrl] = useState('')
    const [isRedirect, setIsRedirect] = useState(false)
    const [currSong, setCurrSong] = useState({})
    const [songList, setSongList] = useState([])
    const [colorRowIndex, setColorRowIndex] = useState(-1)
    const [isSongPage, setIsSongPage] = useState(false)
    const [currUrl, setCurrUrl] = useState('')

   const { id: userId } = currentUser
   
    useEffect(() => {
        getLoggedInUser()
    }, [])

    const getLoggedInUser = async () => {
        let user = JSON.parse(getCredential());
        if( user ){
             Axios.get(`${BASE_URL}/userProfile/${user.id}`)
            .then(res => {
                setCurrentUser(res.data)
            })
        }
    }

     const logoutHandler = async () => {
        removeCredential();
        await setCurrentUser({});
        await setUrl('/');
        await setIsRedirect(true)
        window.location.reload(true);
    }

    const loginHandler = async () => {
        await setUrl('/login')
        await setIsRedirect(true)
        window.location.reload(true);
    }

    const regisHandler = async () => {
        await setUrl('/register')
        await setIsRedirect(true)
        window.location.reload(true);
    }


    const getSideBar = () => {
        return <Sidebar userId={userId} setCurrentUser={setCurrentUser}
            user={currentUser} login={loginHandler} logout={logoutHandler} 
            register={regisHandler} currUrl={currUrl}  />
    }

    const handleGetCurrSongGlobal = () => {
        const currentSong = JSON.parse(localStorage.getItem("currSong"))
        setCurrSong(currentSong)
    }

    const handleGetSongListGlobal = () => {
        const songList = JSON.parse(localStorage.getItem("songList"))
        setSongList(songList)
    }

    const nextSong = () => {
        if (currSong.index + 1 >= songList.length) {
            const songAfter = songList[0]
            const newData = {
                ...songAfter,
                index: 0
            }
            setCurrSong(newData)
            setColorRowIndex(newData.index)
            localStorage.setItem('currSong', JSON.stringify(newData))
        }
        else {
            const songAfter = songList[currSong.index + 1]
            const newData = {
                ...songAfter,
                index: currSong.index + 1
            }
            setCurrSong(newData)
            setColorRowIndex(newData.index)
            localStorage.setItem('currSong', JSON.stringify(newData))
        }
    }

    const prevSong = () => {
        if (currSong.index - 1 < 0) {
            const songBefore = songList[songList.length - 1]
            const newData = {
                ...songBefore,
                index: songList.length - 1
            }
            setCurrSong(newData)
            setColorRowIndex(newData.index)
            localStorage.setItem('currSong', JSON.stringify(newData))
        }
        else {
            const songBefore = songList[currSong.index - 1]
            const newData = {
                ...songBefore,
                index: currSong.index - 1
            }
            setCurrSong(newData)
            setColorRowIndex(newData.index)
            localStorage.setItem('currSong', JSON.stringify(newData))
        }
    }


    return (
            <div className="player">
                <div className="player__body">
                    <Router>
                        {window.location.pathname === '/login' || window.location.pathname === '/register' ? null : getSideBar() }
                            {isRedirect ? <Redirect to={url} /> :
                            <Switch>
                                <Route exact path='/'
                                    render={() => <HomePage
                                        handleGetCurrSongGlobal={() => handleGetCurrSongGlobal()}
                                        handleGetSongListGlobal={handleGetSongListGlobal}
                                        setColorRowIndex={setColorRowIndex}
                                        colorRowIndex={colorRowIndex}
                                        setCurrUrl={setCurrUrl}
                                    />
                                    }
                                />
                                 <Route exact path='/profile/:userId'
                                    render={({match}) => <UserProfile
                                        match={match}
                                        handleGetCurrSongGlobal={() => handleGetCurrSongGlobal()}
                                        handleGetSongListGlobal={handleGetSongListGlobal}
                                        setColorRowIndex={setColorRowIndex}
                                        colorRowIndex={colorRowIndex}
                                        setCurrUrl={setCurrUrl}
                                    />
                                    }
                                />
                                <Route exact path='/profile/:userId/likedSongs'
                                    render={() => <LikedSongList
                                        handleGetCurrSongGlobal={() => handleGetCurrSongGlobal()}
                                        handleGetSongListGlobal={handleGetSongListGlobal}
                                        setColorRowIndex={setColorRowIndex}
                                        colorRowIndex={colorRowIndex}
                                    />
                                    }
                                />
                                {/* <Route exact path='/profile/:userId/playlist/:playlistId' component={PlaylistSong} /> */}
                                <Route exact path='/profile/:userId/playlist/:playlistId'
                                    render={({match}) => <PlaylistSong
                                        match={match}
                                        handleGetCurrSongGlobal={() => handleGetCurrSongGlobal()}
                                        handleGetSongListGlobal={handleGetSongListGlobal}
                                        setColorRowIndex={setColorRowIndex}
                                        colorRowIndex={colorRowIndex}
                                    />
                                    }
                                />
                                <Route exact path='/song-list'
                                    render={() => <SongList
                                        handleGetCurrSongGlobal={() => handleGetCurrSongGlobal()}
                                        handleGetSongListGlobal={handleGetSongListGlobal}
                                        setColorRowIndex={setColorRowIndex}
                                        colorRowIndex={colorRowIndex}
                                        setCurrUrl={setCurrUrl}
                                    />
                                    }
                                />
                                <Route exact path='/songpage/:song'
                                    render={({match}) => <SongPage
                                        match={match}
                                        handleGetCurrSongGlobal={() => handleGetCurrSongGlobal()}
                                        handleGetSongListGlobal={handleGetSongListGlobal}
                                        setColorRowIndex={setColorRowIndex}
                                        colorRowIndex={colorRowIndex}
                                        setIsSongPage={setIsSongPage}
                                    />
                                    }
                                />
                                <Route exact path='/login' component={LoginForm} />
                                <Route exact path={`/register`} component={RegisterForm} />
                                {/* <Route exact path='/admin/manage-songs' component={AdminPage}/> */}
                                <Route exact path='/admin/manage-songs'
                                    render={() => <AdminPage
                                        handleGetCurrSongGlobal={() => handleGetCurrSongGlobal()}
                                        handleGetSongListGlobal={handleGetSongListGlobal}
                                        setColorRowIndex={setColorRowIndex}
                                        colorRowIndex={colorRowIndex}
                                        setCurrUrl={setCurrUrl}
                                    />
                                    }
                                />
                            </Switch>
                            }
                            {/* <div style={{ marginTop: '100px' }}> </div> */}

                            {
                                window.location.pathname === '/login' || window.location.pathname === '/register' ? null :
                                <div style={{ position: 'fixed', bottom: '0'}} className='songplayer-container'>
                                    <SongPlayer
                                        path={currSong.path}
                                        nextPrevBtn={true}
                                        autoPlay={!isSongPage ? true : false}
                                        nextSong={nextSong}
                                        prevSong={prevSong}
                                    />
                                </div>
                            }
                    </Router>
                </div>
        </div>
    )
}

export default Home;
