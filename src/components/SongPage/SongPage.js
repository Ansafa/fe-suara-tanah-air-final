import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import download from 'downloadjs';
import { getCredential, setCredential } from '../Utils';
import SongPlayer from '../AudioPlayer/AudioPlayer';
import SearchSong from '../SearchSong/SearchSong';
import "./SongPage.scss";
import Mountain from "../../public_images/mountain.png";
import UploadedSongList from "../UploadedSongList/UploadedSongList"
import Toast from 'react-bootstrap/Toast';
import { FaMusic } from 'react-icons/fa';
import { BASE_URL } from '../Constant'
import Spinner from 'react-bootstrap/Spinner';

const SongPage = ({ match, handleGetCurrSongGlobal, handleGetSongListGlobal, setColorRowIndex, colorRowIndex }) => {
    const songId = match.params.song
    const [readMore,setReadMore]= useState(false);
    const linkName = readMore ? 'Read less':'Baca Selengkapnya>>'

    const [songInfo, setSongInfo] = useState({})
    const [currentUser, setCurrentUser] = useState({})
    const [showAlert, setShowAlert] = useState(false);
    const [selectedFile, setSelectedFile] = useState({});
    const [isPlayInstrumental, setIsPlayInstrumental] = useState(false)
    const [playlists, setPlaylists] = useState([])
    const [songsCoverUploaded, setSongsCoverUploaded] = useState([{songName: ''}])
    const { pathInstrumental: instrumental } = songInfo
    const [ isShow, setIsShow ] = useState(false);
    const [isReportShow, setIsReportShow] = useState(false)
    const [isAddPlaylistShow, setIsAddPlaylistShow] = useState(false)
    const [uploadToast, setUploadToast] = useState(false)
    const [isUploadSong, setIsUploadSong] = useState(false)
    const [isFetching, setIsFetching] = useState(false)

    const { pathImg } = songInfo

    const getLoggedInUser = async () => {
        let user = JSON.parse(getCredential());
        if (user) {
            Axios.get(`${BASE_URL}/userProfile/${user.id}`)
                .then(res => {
                    setCurrentUser(res.data);
                    getUserPlaylist(res.data.playlists)
                })
        }
    }

    const getSongInfo = async () => {
        Axios.get(`${BASE_URL}/songpage/${songId}`).then(res => {
            setSongInfo(res.data)
        })
    }

    useEffect(() => {
        getLoggedInUser();
        getSongInfo();
        getSongsCoverUpload(songId);
    }, []);

    const getUserPlaylist = (userPlaylists) => {
        userPlaylists.map(playlistId => (
            Axios.get(`${BASE_URL}/playlist/${playlistId}`)
                .then(res => {
                    const data = res.data
                    setPlaylists([...playlists, playlists.push(data)])
                    setPlaylists([...playlists])
                })
        ))
    }


    const addToPlaylist = () => {
            const selectedPlaylist = JSON.parse(document.getElementById("playlistSelect").value);
            const { _id: playlistId } = selectedPlaylist;
            delete selectedPlaylist['index']
            const newSelectedValue = { ...selectedPlaylist }
            const validate = playlistValidation(selectedPlaylist);
            if (validate) {
                Axios.post(`${BASE_URL}/playlist/${playlistId}/song/${songId}`, songInfo)
                    .then(res => {
                        setIsAddPlaylistShow(true)
                        console.log(res)
                    })
            }
    }

    const playlistValidation = (selectedPlaylist) => {
        const { _id: playlistId, index, title, playlistSongs } = selectedPlaylist;
        for (let index = 0; index < playlistSongs.length; index++) {
            if (playlistSongs[index] == songId) {
                setShowAlert(true);
                return false;
            }
        }
        return true;
    }

    const setUser = async () => {
        await Axios.get(`${BASE_URL}/userProfile/${currentUser.id}`).then(res => {
            setCredential(res.data);
        })
        getLoggedInUser();
    }

    const AlertDismissibleExample = () => {
        if (showAlert) {
            const selectedPlaylist = JSON.parse(document.getElementById("playlistSelect").value);
            const { _id: playlistId, index, title } = selectedPlaylist;
            return (
                <div>
                    <h3>{`lagu yang anda ingin masukkan sudah ada di playlist ${title}`}</h3>
                    <button onClick={() => setShowAlert(false)}>OK</button>
                </div>
            );
        }
        return <h1></h1>;
    }

    const fileData = () => {
        if (selectedFile) {
            if (selectedFile.type === "audio/mpeg") {
                return (
                    <div>
                        <h2>File Details:</h2>
                        <p>File Name: {selectedFile.name}</p>
                        <p>File Type: {selectedFile.type}</p>
                    </div>
                );
            } else {
                return (
                    <div>
                        <h2>Please upload file with MP3 format</h2>
                    </div>
                )
            }
        }
    };

    const onFileChange = (e) => {
        setSelectedFile(e.target.files[0]);
    };

    const onFileUpload = async () => {
        if (selectedFile && selectedFile.type === "audio/mpeg") {
            setIsFetching(true)
            const formData = new FormData();
            formData.append(
                "file",
                selectedFile,
                selectedFile.name
            );

            await Axios.post(`${BASE_URL}/song/uploadFile/data`, { 
                uploader: currentUser.id, uploaderEmail: currentUser.email,
                songCover: songInfo._id, songCoverName: songInfo.title, songName: selectedFile.name ,
                songCoverOrigin: songInfo.origin
            })
            .then(res => {
                setIsUploadSong(false)
            });

            await Axios.post(`${BASE_URL}/song/uploadfile`, formData).then(res => {
                getSongsCoverUpload(songId);
                setIsUploadSong(false)
                setIsFetching(false)
                setIsShow(true)
            });
        }else {
            setUploadToast(true)
        }
    };
    const handlePlayInstrumental = () => {
        if (!isPlayInstrumental) {
            setIsPlayInstrumental(true)
        }
        else {
            setIsPlayInstrumental(false)
        }
    }

    const onDownloadSong = async () => {
        const res = await fetch(`${BASE_URL}/song/download/${songInfo._id}`);
        const blob = await res.blob();
        download(blob, songInfo.title + '.mp3');
    }

    const getSongsCoverUpload = (songId) => {
        Axios.get(`${BASE_URL}/list/${songId}/songUploaded`)
        .then(res => {
            const data = res.data[0];
            setSongsCoverUploaded(data.uploadMusic)
        })
    }

    const handleSetGLobalCurrSong = (songData) => {
            localStorage.setItem('currSong', JSON.stringify(songData))
            handleGetCurrSongGlobal()
    }

    return (
        <>
          <div className="w-100 tes">
            {/* <div className="title-songs-city pt-5 customize-background" style={{backgroundImage: `url("data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxIQEBUQEBAPDxUPEBUPDw8QDxAQFQ8QFRUWFhUVFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OFxAQGy0lHR8tLS0tLy8tLS0tLS0tLS0rLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS03LS0tLS0tLf/AABEIALEBHAMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAAAAQIDBAUGB//EAD4QAAICAQAGBgcHAwMFAQAAAAABAhEDBBITITFhBUFRcYGRBhQiMkKhwQdSYnKCsdFT4fAjkvEkM0ODwhb/xAAZAQEBAQEBAQAAAAAAAAAAAAAAAQIEAwX/xAArEQACAgEDAwQBAwUAAAAAAAAAAQISEQMhMQQTURQVQWHwBbHBIjJCcZH/2gAMAwEAAhEDEQA/APfAMZuxw4EOhgMjAqHQwGRgVAMYyMCoKHQUXIwKgodDoZGCNDodBQyMCoKHQxkYI0FEqFQyMCoKJUKhkYFQqJUFDIwRoKJUKhkYFQqJUFDIwRoKJUAyMEKCiVBQyMEaFRMVDIwQoKJ0KhYmCFBROhULDBKgolQUc1z3oKgolQULigqCiVBRbioqHQ6ChYVFQUSAWFRUFDAWFRUFDAWFRUFEgFxUjQ6GBbioqChgLipGgokBLipGgokAuKkaCiQC4qRoKJCFy1I0FEgJclSGqGqSAXFSFBRIC2FSFBqkgFhUBisLOSx11GADLclAAB0LiggHQUW5KAAUFFuSgAFALigDEBbEqMBALCowEAsKjAQCwqMBALCowEAsKjFYCFhUYWILFhUdisVhZLCo7FYWKxYVHYgsQsKjEFisWFRWOyux2c2TqqWWFkLCxktSyx2V2FlyKlljsrsLGRUssLIWFjIqWWFldjsZJUnYWQsLGRUnYWQsdlyKkgI2FixKjAVhYsKDAVhYsKDAiFiwoMDz3pF6Y6LoT1MkpZMn9HClKUecm2lHxd8jyuX7VlrexoknHrcsyUv9qi18z2ho6k1lI85Sgnhs+liPmC+0+be7R1HrSeZvz9kWP7Sc3FYsTXZJyb800afT6ngqcWfUAPnC+1B2v+iTXxSjpLvwi8e7zPW9A+k2jaav9KdTXHDkWrNdy+Jc02ec9PUgstFSTOyAgPKxqgxBYCwoAgAWFCmx2UaxLWPM6qFusOyrWGpFFC2x2U6w9YFoW2OyrWHrDIoWWFldhZMihZY7K7CxkULLCyGsGsMihZYWV2PWGRQnYWQ1g1hkdsnYWQ1g1hkvbJ2FkNY8/wCkvpfg0JOL/wBXLW7DFrd2OcvgXz7EWKlJ4RmSjFZkd3TNLx4YPJlnHHCKuU5tJI+Z+lH2gTy3j0RyxQ4PK905/lXwL59x5Pp7p/Ppk9fPO6fsYo7seJfhj283vOXrWz6eh0qjvPdnztbXttHglOWs7d73bvi2+thBEGycGq3nY5nMkWY4+1aG+Nrr4pfuip5aB5uzuMNs9EzTjrje5rjzNEFwlGTTi7TTacXy7DJgl7Ot1a1Pu7Sx9ddS+R5SludMVtwe06A9PM2CselKWeHDX/8AJFd797x8z6J0X0th0mOvgyRmutcJR/NF70fCdtuVtrnVl+i6XLFNTxTljlH3ZwdP/jkcmpoxlutme8ZH3uws+a9GfaPOCUdJxLJXHJiqMnzcXub7qPbdD9OaPpavBljJ1bg/ZnHvi9/jwOOcJQ5R7xUZbI6dhZGh6vM8rm+0zBrj1zHtR7U6u2zWUa9oPaGPaj2o7TGUbNoG0Me1HtS9oWRs2obQx7QNoO0SyNu1DamLahtR2hZG3aBtTFtA2o7QsjbtA2hi2obUdoWRuWQe0MO0FtB2i3R0NcevzOdtR7YnZLeJ0NbmU6bpuPDB5MuSGOMeMpySXd38jyfT3ptg0a4Re3yfchJasH+OfV3K2fNenOn8+mT1sst0X7GOO6GPuXbze89YdK5c8HPq9XpwWFuz13pJ9ospXj0O8ceDzyXty/JF+73vfyR4CeZybbbbbbbbbcm+LbfFlUn2kXI79PTjBYij5epqy1HmRbrkkyrGmycnS3G2zA5SDW3FaFbICesR1t5EeNf2BVybsObdXVVNEdq066+rmuzvM8Ium+zexbW9z/4POp0dzZI1LMuDTq7TvgT9aXDqfyMWvup/8/3IXW759pKJk7jR0I5Vwb5pjwZpQmpQk4yW9NPV38muD5nNlKv84Eo5BQq1dz6J0J9oGXG1DSYvNFbnLcskfpLxrvPdaL6R6Nlipxz4qf3pxg1ycZNNHwiGkPd11uT60iWu+pnNPpYs7YdY19n2bbD2xwvXw9fPo+jl4Pl+56fk722DbnB9fY/XWX0cie56fk7u3Htzhety7H5MPWpdj8mPRyHucDu7cNucNaRN/DLyYbbJ92Xkx6Nj3OP2dzbhtzibTJ91ijlm927dx9pbh6T7J7kvDO5txbc4cs8k6vjwqM5fNIJ5Zrqm+Sx5H+yHpPse4/TO56wHrBw9rP7s3/65/VFkYzfau9SX0HpV5HuL8HY9ZXaL1lHKeGfavmcTpjp/Ho9x11lmvgg+D/FLgu7iT00V8lX6hJ8RPVaV0lDHFzyTjCK4yk6R8/8AST0wyZ7x4XLFj4Np1PKub+GPLz7Dz3SfSuXSJa2STaXuwXux7l9THbMrTimalrzkixf4kKUiDYLmaPIGyUI9oRVkpOiNlJOVFUpbwsgyAsIykRsSALY9vYRU/wBxSkRSBTXgzUnfxfsZpcQsimZS+TcpZSRYmKXMi2STsoyRkyMZUORFMpPkuRKyqx2ZNZPrixx+7HyGtXqUfJGPVXVreDQO+2Xzf7M+jj7Ph2+jdr93kv5Gp8/JIwKddnkyW0tbrf6WvoKlubVk/wAoayPt/b+THGD7Eu919Bu/wipbmnJn1VcpKK7W0vqZF01guvWMHZ/3I/ycDpjoLPpM9aWbHqr3IKDSgvq+ZlXoY+vPHn7HV/uM4l4PRUxvI9vr7r37+xbhbXm/FHk16NzhGsekZ4flyaq8kzuaJjyQhU57Vr42oxb76495qrMOUfg36/N/IW15/NHPlOd+7X4m3+wpudeym3+WKRqrMXOhLKZtN6RxYY62Wajyb9p90eLPL9M6Vp/BY3CN1eJazfjxXhR5fLhyaz14z1uvWUrffZ4yljg6Yaed2zv9Nelc8twwa2KHByb9uS8PdXdv5nm2Wz0aa3OE1+llTTXFNeB4yy+TqgorgL3CbARg2NEkIbJkFmOdL6lc2CZFshc7YGiMuIyDZQMBCsAnHmN8iFhZCkrFYrBgDkxxkRbECkmJisYAJjIgAfWdRL3dX9/qPVfVXkgSXV+6ITydWt819T6CR8VvA8uOb4Sa+RHHGfxdW5VJPxFLPXHdz3kZaXXUpd29msMy5RL9Zpe75tbwWRLjF/pizPLTKV7lye5/MsjpLfFRX6kMMXjknPK74S8nuGssficb/wA5Edfmn40ReeuEb/VYwXOB50pe665J7mEU1wp98mQ9ab+Fqvwt/QqzaVGC1ppLs9ne+4bpbhNN7cmmTfUn5r6layO/jrsSRRoXSMMsNdLrcarsdb/CmX+sLs8rEXZZRqcaSw9mWd+7va+hmy6PCXGm/n8xy0hdk34f2Es0eFS8E/4Kk0Ycosrej9ie7qtkZ4V8UY+K+pessea8hPLH73Hki5ZEkuDmT6Lg3rKKi+1JHM0j0ec5NqWp2pxpHonG+E0LVS38TMlFrDNRlOLymeZj6Ly/rQr8rCfovOt2WLf5XXnZ6Kc4vddd5GEkuG/xv9zHZh4PX1Op5POL0amvenGvw3v8yX/51f1GvBM7ufSa6pLmU+sKvaruJ2o+CPqZ55PP6T0FJP8A05Ka5+y0c3Joc4upJxfM9Y8seoqzJSXV4kehE3HrJLk8lmg4umQPQZ+j4TfHV8foZsnQ8Um1k39So8ZaMlwdMOqg1ucgC3Lo0o8U+9byqjyaaOlST4ALG4tESFyOwsQADBMQAEhCAA+pxxpb0m+VxfyJxb46ni0jjbacty80D0mceLfhvPpYPg3SO1PSV95LvsSzp/FB8rZx/XrVN+dFW571J+QUUHqM7rySfw42u9MTl2wT/Svkzz7zuP8AO8vx6b2t+DLUXZ2bX3PKP1Ivdv1L/TvOW9Mb4TrvJesS/qLubGBYu6T05Rg/beFtUnTWrz4dR5zJ0jN/6OZ67TuEk4tSXGVt9Wq2uFnX0zWyx1XKG5pq2+Kd/Q8zpOg5Flba1k5NazfFvr3d65bzi6pyiso+l+nqE5Yf5xwdXQ8so24OcGldW5Wk023Fbty3cL4dx6LFpjkrjF5Ny9pb+Ku93amn4nmOj9L2cdRpJ6u/Iqd42l7K3dfG1y40b+j9NfWl7XtVHhxq06664cjHTSw8eT26+GYuXDjt8fnk7m3fXjl3arIPOuuFd6aRnXSddVDfSx37+D5OV5LXpuPsRRly4n1V3URnpuOXvRXkjLPLBvclXLcVGZF+0iuFvkqLcST3+73mbWx9jXiZ5ZUnXH6Fe5lbG/JCD4zXfQkorcpRfejFi0qC4k3pUK6vMya+8FufJLti+5/yZ5Z0uMV4pFGTJHtIa0e2xsMNltxl7qafIU4tdvmVespcKM+bO3wZLG1BsterfXY3qpbnXec+U5EU32mbHt2vs15L6mn4GbIn1peROGTV6yqUzDNxTRXNLsKJYl2F7kJmGkz3TaMrxC2RoaCjNEel2ZnAWoa6INE7aKtQy6o6L3EWoZ7Zq56aOlcmvGgjpddf7GBzXPzIOXM7LHylpI35dMfJ31ULDparfu5HP1hNsljXaWDoy0lMreWHa/OjAyDFirRR1HpMer+5B6Yuz5nOeQpxZ9bjud1xMy1Unhs9I9NlZOrLSu85mmaU21vSpvrd0WV2mSeW21w3NKt5z9VL+nB19FppTya9FajvnUotUk7favqb82muMdyS7FuRydpqqMa9mW+VrWXZW7mvnyIZMt0t0WrppN1SfBP+Tk0tZwi0d/U9MtWcW+Pk6uPS5SipO1d7uzfRa9K3HN0OTuSp7+CrhXHcX6zO7p9ZzjvyfM6vpo6c9lszRtl1jjmXZ8zMB75OaiNT0nsD1tmQdDJKRLZ5b6iFkSJDSiWWRb5kGFA1gGxWDRGiGkNgILBRiaFYWQBqgKxWQpJoi0FhZAITHYwUrYUTEDWTSwADZ4BEkAAjIsiwAppEJcV3nOXB9/1YAcHUf3Hf03B0cvuvuOfD3vD6gBrqeETpeWW5fcx90v3Q5fx+zADiR9KXP/P2L9G9+Pf/APJqnx8QA7ek+T53X/4kevwX1BAB1Q5l+fBxz4j/AK/kAGBs8yLAABRAAEKJiAAEIQADQCGBAREwAhQQABCiAABRiAAQ/9k=")`,backgroundSize:"cover"}}> */}
            <div className="title-songs-city pt-5 customize-background" style={{backgroundImage: `url(${pathImg})`,backgroundSize:"cover"}}>
                <div className='song-page-info ml-2'>
                    <div className="row pl-5">
                        <div className="city-songs">
                            <h4>{songInfo.title}</h4>
                        </div>
                    </div>
                    <div className="row pl-2">
                        <hr className="width-20-height-02 bg-dark mt-0 ml-0 ml-3 mb-0"/>
                    </div>
                    <div className="row pl-5">
                        <div className="city-songs">
                            <h4 className="color-black-opacity-50">{songInfo.origin}</h4>
                        </div>
                    </div>
                </div>
            </div>
        <div className="container choose-playlist pt-2 container-2">
            <div className="row pl-5">
                <h5>Pilih Playlist</h5>
            </div>
            <div className="row pl-5">
                <select id='playlistSelect' className="form-control custom-width-30">
                {
                            playlists.map((playlist, index) => {
                               const data = {
                                   ...playlist,
                                   index
                                }
                                return <option value={JSON.stringify(data)}>{playlist.title}</option>
                            })
                        }
                </select>
                <button type="button" onClick={addToPlaylist} className="btn btn-primary ml-4">Tambah</button>
            </div>
            <div className="">
                <div className="row pt-4">
                    <hr className="w-100 bg-dark mt-0 margin-left-09 mb-0 height-02 margin-right-09"/>
                </div>
                <div className='songPage-wrapper' >
                     <center> <h5 className="">{`Lirik lagu ${songInfo.title}`}</h5> </center>
                    <p className="">{songInfo.lyric}</p>
                </div>
                <div className="row pt-4">
                    <hr className="w-100 bg-dark mt-0 margin-left-09 mb-0 height-02 margin-left-09 margin-right-09"/>
                </div>
                <div className='songPage-wrapper mb-4' >
                        <center> <h5 className="">{`Sejarah lagu ${songInfo.title}`}</h5> </center>

                        <div className="">
                            {readMore && songInfo.history}
                        </div>
                        <div className="row justify-content-center mr-4">
                            <h6 onClick={() => setReadMore(!readMore)}>{linkName}</h6>
                        </div>
                </div>
                <div className="row">
                    <hr className="w-100 bg-dark mt-0 margin-left-09 mb-0 height-02 margin-right-09"/>
                </div>
                </div>
            </div>
             <div className="row pl-5 pt-3 ml-1">
                { currentUser.id &&
                    <>
                        {isUploadSong && <><div className="col-sm-2">
                            <label className="custom-file-upload m-0">
                                <div className="btn btn-secondary"><FaMusic/> Choose File</div>
                                <input type="file" className="upload-song-input" onChange={onFileChange} />
                            </label>
                        </div>
                        <div className='col-sm-3'> <p>{selectedFile.name ? selectedFile.name : 'Choose a file..' }</p> </div></>}
                        <div className="col-sm-1">
                           { isUploadSong ?  
                           <button className="btn btn-primary height-100" onClick={onFileUpload}>
                                Upload
                            </button> : 
                            <button className="btn btn-primary height-100" onClick={() => setIsUploadSong(true)}>
                                Upload
                            </button>
                            }
                        </div>
                    </>
                 }
                 <div className="col-sm-1">
                 <button className="btn btn-primary height-100" onClick={onDownloadSong}>
                     Download
                 </button>
                 </div>
                <div className='ml-5 col-sm-1"'>
                    <button className="btn btn-primary height-100" onClick={() => handleGetCurrSongGlobal(songInfo.path)}>
                        Mainkan Lagu
                    </button>
                </div>
            </div>

            <div className="toast-notification position-toast">
					<Toast onClose={() => setIsShow(false)} show={isShow} delay={3000} autohide>
						<Toast.Body>Anda berhasil mengupload lagu!</Toast.Body>
					</Toast>
			</div>
            {
                songsCoverUploaded[0].songName ?
                    <>
                       { !isFetching ?
                            <UploadedSongList
                                userId={ currentUser ? currentUser.id : null}
                                songId={songId}
                                uploadedSongs={songsCoverUploaded}
                                getSongsCoverUpload={getSongsCoverUpload}
                                isSongPage={true}
                                setIsReportShow={setIsReportShow}
                                handleGetCurrSongGlobal={handleGetCurrSongGlobal}
                                handleGetSongListGlobal={handleGetSongListGlobal}
                                setColorRowIndex={setColorRowIndex}
                                colorRowIndex={colorRowIndex}
                            />
                            :
                            <center className='mt-5'> <Spinner variant='dark'  animation="border" /> </center>
                        }
                    </>
                 :
                null
            }
            <div className="toast-notification position-toast">
				<Toast onClose={() => setIsShow(false)} show={isShow} delay={3000} autohide>
					<Toast.Body>Anda berhasil mengupload lagu!</Toast.Body>
				</Toast>
			</div>
            <div className="toast-notification position-toast">
				<Toast onClose={() => setIsReportShow(false)} show={isReportShow} delay={3000} autohide>
					<Toast.Body>Anda berhasil melaporkan lagu!</Toast.Body>
				</Toast>
			</div>
            <div className="toast-notification position-toast">
				<Toast onClose={() => setIsAddPlaylistShow(false)} show={isAddPlaylistShow} delay={3000} autohide>
					<Toast.Body>Anda berhasil menambahkan lagu ke playlist!</Toast.Body>
				</Toast>
			</div>
            <div className="toast-notification position-toast">
					<Toast onClose={() => setUploadToast(false)} show={uploadToast} delay={3000} autohide>
						<Toast.Body>Mohon untuk memasukan file dengan format mp3</Toast.Body>
					</Toast>
			</div>
            <div style={{height: '120px'}}> </div>
        </div>
    </>
  )
};

export default SongPage;
