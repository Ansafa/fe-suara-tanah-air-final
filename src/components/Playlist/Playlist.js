import React, { useEffect, useState } from 'react';
import { useHistory, Link } from "react-router-dom";
import { createBrowserHistory } from "history";
import PlaylistSong from '../PlaylistSong/PlaylistSong';
import './Playlist.scss'
import Axios from 'axios';
import Mountain from '../../public_images/mountain.png';
import { BASE_URL } from '../Constant'

const Playlist = ({playlistId, currentUser}) => {
    const [playlist, setPlaylist] = useState({})
    const [playlistSongs, setPlaylistSongs] = useState([])
    const [isShowing, setIsShowing] = useState(false)

    const { title, imgPath } = playlist
    const history = useHistory();

    useEffect(() => {
        getPlaylist()
    }, [])

    const getPlaylist = () => {
        Axios.get(`${BASE_URL}/playlist/${playlistId}`)
        .then(res => {
            setPlaylist(res.data)
        })
    }

    const redirectToPlaylistPage = () => {
       history.push({
            pathname: `/profile/${currentUser.id}/playlist/${playlistId}`,
            state: {currentUser }
        })
    }

    return (
        <>
            <div style={{cursor: 'pointer'}} onClick={redirectToPlaylistPage} className="col-sm-4 text-center upwards-content">
                <img src={imgPath ? imgPath : Mountain} class="rounded custom-height-width-image" alt="Lagu Favorit"/>
                <h4 className="font-family--helvetica">{title}</h4>
            </div>
        </>
    )
}

export default Playlist
